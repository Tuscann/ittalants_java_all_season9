package Lecture_07;

public class _06_ {
    public static void main(String[] args) {
        char[][] matrix = new char[][]{
                {'h', 't', 'j', 'v', 'm'},
                {'y', 'r', 'w', 'l', 'o'},
                {'m', 'n', 'q', 'c', 'p'},
                {'g', 'f', 'h', 'g', 'a'},
                {'o', 'z', 'm', 'd', 's'}

        };

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (row == col) {
                    System.out.print(matrix[row][col] + " ");
                }
            }
        }
    }
}
