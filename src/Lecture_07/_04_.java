package Lecture_07;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        System.out.print("Write length of array : ");
        Scanner scan = new Scanner(System.in);

        int x = Integer.parseInt(scan.nextLine());
        int[] array = new int[x];

        for (int i = 0; i < array.length; i++) {
            System.out.print("Add 0 or 1 : ");
            int current = Integer.parseInt(scan.nextLine());
            if (current == 0 || current == 1) {
                array[i] = current;
            } else {
                System.out.print("Add 0 or 1 : ");
                current = Integer.parseInt(scan.nextLine());
                i--;
            }
        }
        System.out.print("Array before: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                int current = array[i];
                array[i] = array[i + 1];
                array[i + 1] = current;
                i = -1;
            }
        }
        System.out.println();
        System.out.print("Array after : ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
