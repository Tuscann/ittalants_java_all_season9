package Lecture_07;

public class _01_ {
    public static void main(String[] args) {

        int[][] matrix = new int[][]{
                {48, 72, -13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, -33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
                {61, 69, 63, 64, 65},
        };
        boolean isFound = false;
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (matrix[row][col] < 0) {
                    System.out.printf("Found number %d < under 0",matrix[row][col]);
                    isFound = true;
                }
                if (isFound) {
                    break;
                }
            }
            if (isFound) {
                break;
            }
        }
        if (isFound == false) {
            System.out.println("All numbers are bigger than 0");
        }
    }
}
