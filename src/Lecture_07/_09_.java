package Lecture_07;

public class _09_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {48, 72, -13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, -33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
                {61, 69, 63, 64, 65},
                {61, 69, 63, 64, 165},
        };

        int[][] maxMatrix = new int[2][2];
        int maxSum = 0;

        for (int row = 0; row < matrix.length-1; row++) {
            for (int col = 0; col < matrix[0].length-1; col++) {
                int currentSum = matrix[row][col] + matrix[row][col + 1] + matrix[row + 1][col] + matrix[row + 1][col + 1];

                if (currentSum > maxSum) {

                    maxSum = currentSum;

                    maxMatrix[0][0] = matrix[row][col];
                    maxMatrix[0][1] = matrix[row][col + 1];
                    maxMatrix[1][0] = matrix[row + 1][col];
                    maxMatrix[1][1] = matrix[row + 1][col + 1];
                }
            }
        }

        System.out.println("Max matrix 2 x 2 : ");
        for (int row = 0; row < maxMatrix.length; row++) {
            for (int col = 0; col < maxMatrix.length; col++) {
                System.out.print(maxMatrix[row][col] + " ");
            }
            System.out.println();
        }


    }
}
