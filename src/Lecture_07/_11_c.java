package Lecture_07;

import java.util.Scanner;

public class _11_c {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter rows : ");
        int rows = Integer.parseInt(sc.nextLine());

        System.out.print("Enter columns : ");
        int cols = Integer.parseInt(sc.nextLine());

        int[][] matrix = new int[rows][cols];

        int startRow = 0;
        int startCol = 0;
        int row = 0;
        int col = 0;

        for (int i = 0; i < rows * cols; i++) {
            matrix[row][col] = i + 1;
            row--;
            col++;
            if (row < 0 || col >= cols) {
                startRow++;
                if (startRow >= rows) {
                    startRow = rows - 1;
                    startCol++;
                }
                row = startRow;
                col = startCol;
            }
        }

        for (int rowss = 0; rowss < matrix.length; rowss++) {
            for (int columnss = 0; columnss < matrix[rowss].length; columnss++) {
                System.out.print(matrix[rowss][columnss] + " ");
            }
            System.out.println();
        }
    }
}
