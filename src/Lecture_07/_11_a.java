package Lecture_07;

import java.util.Scanner;

public class _11_a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Add rows : ");
        int rows = Integer.parseInt(scan.nextLine());
        System.out.print("Add column : ");
        int columns = Integer.parseInt(scan.nextLine());

        int[][] matrix = new int[rows][columns];
        int counter = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[0].length; column++) {
                matrix[row][column] = ++counter;

                System.out.print(matrix[row][column] + " ");
            }
            System.out.println();
        }
    }
}
