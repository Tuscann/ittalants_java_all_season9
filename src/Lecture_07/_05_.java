package Lecture_07;

public class _05_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {48, 72, -13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, -33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
                {61, 69, 63, 64, 65},
        };

        int maxRow = 0;
        for (int row = 0; row < matrix.length; row++) {
            int currentRowSum = 0;
            for (int col = 0; col < matrix[0].length; col++) {
                currentRowSum += matrix[row][col];
            }
            if (currentRowSum > maxRow) {
                maxRow = currentRowSum;
            }
        }
        System.out.println("Max sum = " + maxRow);
    }
}
