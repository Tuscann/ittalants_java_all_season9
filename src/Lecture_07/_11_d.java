package Lecture_07;

import java.util.Scanner;

public class _11_d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Add rows : ");
        int rows = Integer.parseInt(scan.nextLine());
        System.out.print("Add column : ");
        int columns = Integer.parseInt(scan.nextLine());

        int[][] matrix = new int[rows][columns];

        int currentRow = 0;
        int currentCol = 0;

        for (int i = 0; i < rows * columns; i++) {
            matrix[currentRow][currentCol] = i + 1;
            if (currentCol % 2 == 0) {
                currentRow++;
                if (currentRow == matrix.length) {
                    currentRow--;
                    currentCol++;
                }
            }
            else if(currentCol % 2 != 0) {
                currentRow--;
                if (currentRow == -1) {
                    currentRow++;
                    currentCol++;
                }
            }
        }
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                System.out.print(matrix[row][column] + " ");
            }
            System.out.println();
        }
    }
}
