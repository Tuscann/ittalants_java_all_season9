package Lecture_07;

public class _08_ {
    public static void main(String[] args) {
        boolean[][] matrix = new boolean[][]{
                {true, true, true, true, false},
                {true, true, true, false, true},
                {true, true, false, true, true},
                {true, false, true, true, true},
                {false, false, false, false, true},
        };
        boolean hasTrue = true;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (row < matrix.length - col - 1 && !matrix[row][col]) {
                    hasTrue = false;
                    break;
                }
            }
            if (hasTrue == false) {
                break;
            }
        }
        if (hasTrue) {
            System.out.println("All values are true");
        } else {
            System.out.println("Not all values are true");
        }
    }
}
