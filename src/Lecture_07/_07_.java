package Lecture_07;

public class _07_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {48, 72, -13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, -33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
                {61, 69, 63, 64, 65},
        };

        long sum = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (row < col) {
                    sum += matrix[row][col];
                }
            }
        }
        System.out.println("Sum = "+sum );
    }
}
