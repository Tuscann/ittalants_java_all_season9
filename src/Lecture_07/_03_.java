package Lecture_07;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {
        char[] matrix = new char[]{
                'h', 't', 'j', 'v', 'm'
        };

        Scanner scan = new Scanner(System.in);
        System.out.print("Write searched char symbol : ");
        char searchChar = scan.next().charAt(0);
        boolean isFound = true;

        for (int row = 0; row < matrix.length; row++) {
            if (searchChar == matrix[row]) {
                System.out.printf("Index %c of %d",  searchChar,row);
                isFound = false;
                break;
            }
        }
        if (isFound){
            System.out.println("Not found char in array");
        }
    }
}
