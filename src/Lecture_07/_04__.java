package Lecture_07;

public class _04__ {
    public static void main(String[] args) {

        int[] arr = {0, 1, 0, 0, 1};
        //count zeroes
        //place as many zeroes as counted on first zeroes cells
        //place ones on the rest of the cells

        int countZeroes = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                countZeroes++;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (i < countZeroes) {
                arr[i] = 0;
            } else {
                arr[i] = 1;
            }
            System.out.print(arr[i] + " ");
        }
    }
}
