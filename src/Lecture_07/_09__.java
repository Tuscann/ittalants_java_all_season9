package Lecture_07;

public class _09__ {
    public static void main(String[] args) {
        int[][] matrix = {
                {14, 7, 1, 5},
                {2, 11, 4, 9},
                {3, 3, 8, 6},
                {0, 7, 1, 345}
        };

        int maxSum = 0;
        int maxI = 0;
        int maxJ = 0;
        for (int row = 0; row < matrix.length - 1; row++) {
            for (int column = 0; column < matrix[row].length - 1; column++) {
                int sum = matrix[row][column] + matrix[row][column + 1] + matrix[row + 1][column] + matrix[row + 1][column + 1];

                if (sum > maxSum) {
                    maxSum = sum;
                    maxI = row;
                    maxJ = column;
                }
            }
        }
        System.out.println(matrix[maxI][maxJ] + " " + matrix[maxI][maxJ + 1]);
        System.out.println(matrix[maxI + 1][maxJ] + " " + matrix[maxI + 1][maxJ + 1]);
        System.out.println("Sum of matrix : " + maxSum);
    }
}
