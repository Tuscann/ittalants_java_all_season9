package Lecture_07;

public class _10_ {
    public static void main(String[] args) {
        int[] arr = {4, 3, 7, 45, 3, 6, 5, 4, 8, 4, 5, 2, 5, 5, 9, 36, 5, 4, 658, 4, 36, 34, 6, 58};

        int maxCount = 1;
        int maxTarget = arr[0];

        for (int i = 0; i < arr.length; i++) {
            int currentNumber = arr[i];
            int count = 1;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] == currentNumber) {
                    count++;
                }
            }
            if (count > maxCount) {
                maxCount = count;
                maxTarget = currentNumber;
            }
        }
        System.out.println("Most frequent number is " + maxTarget + " (" + maxCount + " times)");
    }
}
