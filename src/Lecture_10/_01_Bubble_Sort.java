package Lecture_10;

public class _01_Bubble_Sort {
    public static void main(String[] args) {
        int[] bubbleSort2 = new int[]{4, 6, 7, 8, 1, 2, 3, 0, 5, 6};
        int[] bubbleSort = new int[]{1, 2, 3, 4, 6, 7, 8, 0, 15, 16};

        printBubble(bubbleSort);
        BubbleSort(bubbleSort);
        printBubble(bubbleSort);
    }

    private static void BubbleSort(int[] bubbleSort) {
        int totalIteration = 0;
        int counterSwaps = 0;
        int element;
        int size = bubbleSort.length;

        for (int i = 1; i < size; i++) {
            for (int j = size - 1; j >= i; j--) {
                if (bubbleSort[j] < bubbleSort[j - 1]) {
                    element = bubbleSort[j - 1];
                    bubbleSort[j - 1] = bubbleSort[j];
                    bubbleSort[j] = element;
                    counterSwaps++;
                }
                totalIteration++;
            }
        }
        System.out.println("Swaps counter = " + counterSwaps);
        System.out.println("Iteration iteration = " + totalIteration);
    }

    private static void printBubble(int[] bubbleSort) {
        for (int i = 0; i < bubbleSort.length; i++) {
            System.out.print(bubbleSort[i] + " ");
        }
        System.out.println();
    }
}
