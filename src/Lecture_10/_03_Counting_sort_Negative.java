package Lecture_10;

public class _03_Counting_sort_Negative {
    public static void main(String[] args) {
        int[] array = new int[]{2, -5, -3, 1, 3};
        System.out.print("Starting array : ");
        printtt(array);

        int minValue = array[0];
        int maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            } else if (array[i] > maxValue) {
                maxValue = array[i];
            }
        }
        sort(array, minValue, maxValue);

        System.out.print("Sorted array   : ");
        printtt(array);
    }

    private static void sort(int[] array, Integer minValue, Integer maxValue) {
        int[] buckets = new int[maxValue - minValue + 1];   // 9

        for (int i = 0; i < array.length; i++) {
            buckets[array[i] - minValue]++;
        }
        // ako dadeno chislo go ima na neviq indeks slagama 1;

        int sortedIndex = 0;
        for (int i = 0; i < buckets.length; i++) {
            while (buckets[i] > 0) {
                array[sortedIndex++] = i + minValue;
                //sortedIndex = Slaga na tekushta podredena stoinost i posle uvelichava sas edno za sleddvashtata iteraziq
                // i + minValue = tekushtniq index plus nai malkoto chislo = ravno na parvonacholnoto chislo

                buckets[i]--; // zanulqva stoinostite vav bucket masiv
            }
        }
    }

    private static void printtt(int[] index) {
        for (int i = 0; i < index.length; i++) {
            System.out.print(index[i] + " ");
        }
        System.out.println();
    }
}
