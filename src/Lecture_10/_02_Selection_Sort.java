package Lecture_10;

public class _02_Selection_Sort {
    public static void main(String[] args) {
        int[] selectionSort2 = new int[]{4, 6, 7, 8, 1, 2, 3, 0, 5, 6};
        int[] selectionSort = new int[]{1, 2, 3, 4, 6, 7, 8, 0, 15, 16};

        printBubble(selectionSort);
        SelectionSort(selectionSort);
        printBubble(selectionSort);
    }

    private static void SelectionSort(int[] selectionSort) {
        int counterSwaps = 0;
        int totalIteration = 0;

        for (int i = 0; i < selectionSort.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < selectionSort.length; j++) {
                if (selectionSort[j] < selectionSort[index]) {
                    index = j;
                    counterSwaps++;
                }
                totalIteration++;
            }
            int smallerNumber = selectionSort[index];
            selectionSort[index] = selectionSort[i];
            selectionSort[i] = smallerNumber;
        }

        System.out.println("Total swaps = " + counterSwaps);
        System.out.println("Total iteration = " + totalIteration);
    }

    private static void printBubble(int[] bubbleSort) {
        for (int i = 0; i < bubbleSort.length; i++) {
            System.out.print(bubbleSort[i] + " ");
        }
        System.out.println();
    }
}
