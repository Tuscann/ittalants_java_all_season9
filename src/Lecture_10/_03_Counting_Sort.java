package Lecture_10;

public class _03_Counting_Sort {
    public static void main(String[] args) {
        int[] array = new int[]{4, 5, 6, 3, 1, 8, 4, 9, 2};
        System.out.print("Starting array : ");
        printtt(array);

        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        int[] helper = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            helper[array[i]]++;
        }
        System.out.print("Helper array   : ");
        printtt(helper);

        int startCell = 0;

        for (int i = 0; i < helper.length; i++) {
            while (helper[i] > 0) {
                array[startCell] = i;
                helper[i]--;
                startCell++;
            }
        }
        System.out.print("Sorted array   : ");
        printtt(array);
    }

    private static void printtt(int[] index) {
        for (int i = 0; i < index.length; i++) {
            System.out.print(index[i] + " ");
        }
        System.out.println();
    }
}
