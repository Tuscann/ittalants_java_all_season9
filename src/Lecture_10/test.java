package Lecture_10;

public class test {
    public static void main(String[] args) {
        int[] bubbleSort2 = new int[]{4, 6, 7, 8, 1, 2, 3, 0, 5, 6};
        int[] bubbleSort = new int[]{1, 2, 3, 4, 6, 7, 8, 0, 15, 16};

        printBubble(bubbleSort);
        BubbleSort(bubbleSort);
        printBubble(bubbleSort);
    }

    private static void BubbleSort(int[] bubbleSort) {
        for (int i = 0; i < bubbleSort.length; i++) {
            for (int j = 1; j < bubbleSort.length-i; j++) {
                if (bubbleSort[j - 1] > bubbleSort[j]) {
                    int temp = bubbleSort[j - 1];
                    bubbleSort[j - 1] = bubbleSort[j];
                    bubbleSort[j] = temp;
                }
            }
        }
    }

    private static void printBubble(int[] bubbleSort) {
        for (int i = 0; i < bubbleSort.length; i++) {
            System.out.print(bubbleSort[i] + " ");
        }
        System.out.println();
    }
}
