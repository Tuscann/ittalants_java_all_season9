package Lecture_10;

import java.util.Arrays;

public class _04_Radix_Sort {
    public static void main(String[] args) {

        int[] startArray = new int[]{3213, 1322, 123, 221, 9731, 233, 92, 23, 9, 85181};  // using counting sort but for big numbers diapozon
        System.out.println("Start array  : " + Arrays.toString(startArray));
        int[] result = radixSort(startArray);
        System.out.println("Sorted array : " + Arrays.toString(result));
    }

    private static int[] radixSort(int nums[]) {
        // намираме максималния брой цифри
        int maxDigits = getNumOfDigits(getMaxNum(nums));
        // сортираме последователно по всички цифри
        for (int digit = 1; digit <= maxDigits; digit++) {
            nums = countingSort(nums, digit);
        }
        return nums;
    }

    private static int[] countingSort(int[] nums, int digit) {

        int numbersCount[] = new int[10];    //помощния масив е от 0 до 9
        int result[] = new int[nums.length];
        for (int index = 0; index < nums.length; index++) {
            // вместо числото, ще извлечем определенацифра от него
            int num = kthDigit(nums[index], digit);
            numbersCount[num]++;
        }
           // намираме крайната позиция на всеки елемент като добавяме броя на всички преди него към неговия брой
        for (int index = 1; index < numbersCount.length; index++) {
            numbersCount[index] += numbersCount[index - 1];
        }
           // тук ще записваме всеки елемент на правилната му        позиция
        for (int index = nums.length - 1; index >= 0; index--) {
            int num = nums[index];
            // вземаме определена цифра на всяко число и по нея гледаме позицията му в масива
            int numDigit = kthDigit(num, digit);
            numbersCount[numDigit]--;
            int pos = numbersCount[numDigit];
            result[pos] = num;
        }
        System.out.println(Arrays.toString(result));
        return result;
    }

    static int kthDigit(int num, int k) {

           // намираме 10 на степен к-1, за да махнем първите к-1 цифири
        int tenOnKth = (int) Math.pow(10, k - 1);
           // делим и връщаме к-тата цифра като резултат
        return (num / tenOnKth) % 10;
    }

    // метод, който ни връща най-големия елемент в масив
    static int getMaxNum(int nums[]) {
        int max = nums[0];
        for (int index = 1; index < nums.length; index++) {
            if (nums[index] > max)
                max = nums[index];
        }
        return max;
    }

    static int getNumOfDigits(int num) {
        int digitCount = 0;
        while (num > 0) {
            num /= 10;
            digitCount++;
        }
        return digitCount;
    }
}