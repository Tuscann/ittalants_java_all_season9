package Lecture_10;

public class _01_Bubble_Sort_Optimization {
    public static void main(String[] args) {
        int[] bubbleSort2 = new int[]{4, 6, 7, 8, 1, 2, 3, 0, 5, 6};
        int[] bubbleSort = new int[]{1, 2, 3, 4, 6, 7, 8, 0, 15, 16};

        printBubble(bubbleSort);
        BubbleSort(bubbleSort);
        printBubble(bubbleSort);
    }

    private static void BubbleSort(int[] bubbleSort) {
        int counterSwaps = 0;
        int totalIteration = 0;
        int element;
        int size = bubbleSort.length;

        for (int i = 1; i < size; i++) {
            boolean flag = true;
            for (int j = size - 1; j >= i; j--) {
                if (bubbleSort[j - 1] > bubbleSort[j]) {
                    flag = false;
                    element = bubbleSort[j - 1];
                    bubbleSort[j - 1] = bubbleSort[j];
                    bubbleSort[j] = element;
                    counterSwaps++;
                }
                totalIteration++;
            }
            if (flag) {
                break;
            }
        }
        System.out.println("Counter swaps = " + counterSwaps);
        System.out.println("Iteration counter = " + totalIteration);
    }

    private static void printBubble(int[] bubbleSort) {
        for (int i = 0; i < bubbleSort.length; i++) {
            System.out.print(bubbleSort[i] + " ");
        }
        System.out.println();
    }
}
