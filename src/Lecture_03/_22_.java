package Lecture_03;

import java.util.Scanner;

public class _22_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number : ");
        int n = scan.nextInt();
        int counter = 1;

        if (n >= 1 && n <= 999) {

            while(counter <= 10) {
                n++;
                if(n % 2 == 0 || n % 3 == 0 || n % 5 == 0) {
                    if(counter == 1) {
                        System.out.println(counter + ":" + n + "; ");
                    }
                    else if(counter == 10){
                        System.out.println(counter + ":" + n);
                    }
                    else {
                        System.out.println(counter + ":" + n + ", ");
                    }
                    counter++;
                }
            }
        }
    }
}
