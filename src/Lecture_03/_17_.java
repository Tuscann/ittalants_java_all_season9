package Lecture_03;

import java.util.Scanner;

public class _17_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        char c = scan.next().charAt(0);

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row == 0 || row == n - 1) {
                    System.out.print("*");
                }
                else {
                    if (col == 0 || col == n - 1) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(c);
                    }
                }
            }
            System.out.println();
        }
    }
}
