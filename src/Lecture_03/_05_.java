package Lecture_03;

import java.util.Scanner;

public class _05_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add first number : ");
        int first = scan.nextInt();
        System.out.println("Add second number : ");
        int second = scan.nextInt();

        if (first > second) {
            for (int i = second; i <= first; i++) {
                System.out.print(i + " ");
            }
        } else {
            for (int i = first; i <= second; i++) {
                System.out.print(i + " ");
            }
        }
    }
}
