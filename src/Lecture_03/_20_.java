package Lecture_03;

import java.util.Scanner;

public class _20_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int counter = 0;

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 10; col++) {
                counter++;
                if (counter == 10) {
                    counter = 0;
                }
                System.out.print(counter + " ");
            }
            counter = row + 2;
            System.out.println();
        }
    }
}
