package Lecture_03;

import java.util.Scanner;

public class _19_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        if (n >= 10 && n <= 99) {
            while (true) {
                if (n % 2 == 0) {
                    n /= 2;
                } else {
                    n = 3 * n + 1;
                }
                System.out.print(n + " ");

                if (n == 1) {
                    break;
                }
            }
        }
    }
}
