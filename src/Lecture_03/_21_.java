package Lecture_03;

import java.util.Scanner;

public class _21_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number between 1 nad 52");
        int n = scan.nextInt();

        if (n >= 1 && n <= 52) {
            for (int i = 1; i <= 52; i++) {
                if (i > n - 1) {
                    int cardNumber = (i - 1) / 4 + 1;  // magic

                    switch(cardNumber){
                        case 1: System.out.print(2); break;
                        case 2: System.out.print(3); break;
                        case 3: System.out.print(4);break;
                        case 4: System.out.print(5); break;
                        case 5: System.out.print(6); break;
                        case 6: System.out.print(7); break;
                        case 7: System.out.print(8); break;
                        case 8: System.out.print(9); break;
                        case 9: System.out.print(10); break;
                        case 10: System.out.print("Jack"); break;
                        case 11: System.out.print("Queen"); break;
                        case 12: System.out.print("King"); break;
                        case 13: System.out.print("Ace"); break;
                    }

                    if (i % 4 == 0) {
                        System.out.print(" Clubs");
                    } else if (i % 4 == 1) {
                        System.out.print(" Spades");
                    } else if (i % 4 == 2) {
                        System.out.print(" Diamonds");
                    } else if (i % 4 == 3) {
                        System.out.print(" Hearts");
                    }
                    if(i < 52){
                        System.out.print(", ");
                    }
                }
            }
        }
    }
}
