package Lecture_03;

import java.util.Scanner;

public class _10_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the value of n:");
        int number = scanner.nextInt();
        scanner.close();

        boolean isPrime = false;

        for(int i = 2; i < number / 2; i++) {
            if(number % i != 0) {
                isPrime = true;
                break;
            }
        }

        if(isPrime) {
            System.out.println("Number " + number + " is prime.");
        } else {
            System.out.println("Number " + number + " is not prime.");
        }
    }
}
