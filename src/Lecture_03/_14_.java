package Lecture_03;

import java.util.Scanner;

public class _14_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a integer : ");
        int n = scan.nextInt();

        for (int i = 200; i >= 10; i--) {
            if ((i % 7 == 0) && (i < n)) {
                System.out.print(i + " ");
            }

        }
    }
}
