package Lecture_03;

import java.util.Scanner;

public class _16_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number :");
        int a = scan.nextInt();
        System.out.println("Write a number :");
        int b = scan.nextInt();

        if (a >= 10 && a <= 5555 && b >= 10 && b <= 5555) {

            int max = a;
            int min = b;
            if (b > a) {
                max = b;
                min = a;
            }
            int counter = 0;

            for (int i = max; i >= min; i--) {
                if (i % 50 == 0) {
                    if (counter == 0) {
                        System.out.print(i);
                        counter++;
                    } else {
                        System.out.print(", " + i);
                    }
                }
            }
            System.out.println(".");
        }
    }
}
