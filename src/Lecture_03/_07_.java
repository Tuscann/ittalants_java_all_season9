package Lecture_03;

import java.util.Scanner;

public class _07_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add number : ");
        int n = scan.nextInt();

        for (int i = 1; i <= n; i++) {
            if (i < n) {
                System.out.print(i * 3 + ",");
            } else {
                System.out.println(i * 3);
            }
        }
    }
}
