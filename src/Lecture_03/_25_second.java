package Lecture_03;

import java.util.Scanner;

public class _25_second {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number:");
        int number = sc.nextInt();
        int factorial = 1;

        do {
            factorial *= number;
            number--;
        } while (number > 0);

        System.out.printf("Factorial of !%d = %d", number, factorial);
    }
}
