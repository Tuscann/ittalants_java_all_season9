package Lecture_03;

public class _12_ {
    public static void main(String[] args) {
        for (int i = 100; i <= 999; i++) {
            int firstDigit = i / 100;
            int secondDigit = i / 10 % 10;
            int thirdDigit = i % 10;

            if (thirdDigit != firstDigit && thirdDigit != secondDigit && firstDigit != secondDigit) {
                System.out.println(i);
            }
        }
    }
}
