package Lecture_03;

import java.util.Scanner;

public class _15_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number : ");
        int n = scan.nextInt();
        int sum = 0;
        int i = 1;

        do {
            sum += i;
            i += 1;
        }
        while (i <= n);
        System.out.println("Sum of the numbers : " + sum);
    }
}
