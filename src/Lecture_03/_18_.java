package Lecture_03;

import java.util.Scanner;

public class _18_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number :");
        int a = scan.nextInt();
        System.out.println("Write a number :");
        int b = scan.nextInt();

        if(a > b) {
            a ^= b;
            b ^= a;
            a ^= b;
        }

        if (a >= 1 && a <= 9 && b >= 1 && b <= 9) {
            for (int i = 1; i <= a; i++) {
                for (int j = 1; j <= b; j++) {
                    System.out.printf("%d * %d = %d;\n", i, j, i * j);
                }
            }
        }
    }
}
