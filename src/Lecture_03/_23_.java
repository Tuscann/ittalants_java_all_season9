package Lecture_03;

public class _23_ {
    public static void main(String[] args) {

        for (int row = 1; row < 10; row++) {
            for (int col = 1; col < 10; col++) {
                if (row < col + 1) {
                    System.out.printf("%d*%d; ", row, col);
                }
            }
            System.out.println();
        }
    }
}
