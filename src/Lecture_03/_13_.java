package Lecture_03;

import java.util.Scanner;

public class _13_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add sum : ");
        int sum = scan.nextInt();

        int counter = 0;

        if (sum >= 2 && sum <= 27) {
            for (int i = 100; i <= 999; i++) {
                int firstDigit = i / 100;
                int secondDigit = i / 10 % 10;
                int thirdDigit = i % 10;

                if (firstDigit + secondDigit + thirdDigit == sum) {
                    if (counter == 0) {
                        System.out.print(i);
                        counter++;
                    } else {
                        System.out.print(", " + i);
                    }
                }
            }
            System.out.print(".");
        }

    }
}
