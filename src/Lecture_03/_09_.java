package Lecture_03;

import java.util.Scanner;

public class _09_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write A : ");
        int a = scan.nextInt();
        System.out.println("Write B : ");
        int b = scan.nextInt();

        int max = a;
        int min = b;
        if (b > a) {
            max = b;
            min = a;
        }
        int sum = 0;

        for (int i = min; i <= max; i++) {
            if (i % 3 == 0) {
                System.out.print("skip 3");
            }
            else {
                System.out.print(i * i);
                sum += i * i;
            }
            if (sum > 200) {
                break;
            }
            System.out.print(" ,");
        }
    }
}
