package Lecture_03;

import java.util.Scanner;

public class _25_factorel {
    public static void main(String[] args) {
        int factorial = 1;
        Scanner scan = new Scanner(System.in);
        System.out.println("Add number : ");
        int number = scan.nextInt();

        int i = 1;
        do {
            factorial *= i;
            i+=1;
        }
        while (i <= number);

        System.out.printf("Factorial of !%d = %d", number, factorial);
    }
}
