package Lecture_03;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter number : ");
        int number = scan.nextInt();

        for (int row = 0; row < number; row++) {
            for (int col= 0; col < number; col++) {
                System.out.print(number + (2 * row) - 1);
            }
            System.out.println();
        }
    }
}
