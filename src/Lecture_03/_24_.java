package Lecture_03;

import java.util.Scanner;

public class _24_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add number : ");
        int number = Integer.parseInt(scan.nextLine());

        int newNumber = 0;
        int nextDigit = number;

        if (number >= 10 && number <= 30000) {
            do {
                newNumber = newNumber * 10 + nextDigit % 10;
                nextDigit /= 10;
            } while (nextDigit > 0);

            if (newNumber == number) {
                System.out.println("The number is palindrome.");
            } else {
                System.out.println("The number is not palindrome.");
            }
        }


    }
}
