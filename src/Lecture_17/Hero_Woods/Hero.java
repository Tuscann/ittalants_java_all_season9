package Lecture_17.Hero_Woods;

import Lecture_17.Hero_Woods.items.Helm;
import Lecture_17.Hero_Woods.items.Shield;
import Lecture_17.Hero_Woods.items.Sword;

public class Hero {
    private static final int MAX_HEALTH = 100;
    private final String name;
    private int health = MAX_HEALTH;
    private final int dmg = 10;
    private final int def = 10;
    private Sword sword;
    private Helm helmet;
    private Shield shield;

    public Hero(String name) {
        this.name = name;
    }

    void fightBoss(Devil devil){

    }

    void fight(Creep creep){

    }
}
