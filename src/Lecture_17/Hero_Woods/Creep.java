package Lecture_17.Hero_Woods;

import java.util.Random;

public class Creep {

    private final String name;
    private int health;
    private int dmg = 10;
    private int def = 10;

    public Creep(String name) {
        this.name = name;
        Random r = new Random();
        this.health = r.nextInt(20) + 10;
        this.dmg = r.nextInt(4) + 1;
        this.def = r.nextInt(4) + 1;
    }


    @Override
    public String toString() {
        return "Name : " + name + "\nHealth : " + health + "\nDamage : " + dmg + "\nDefence : " + def;
    }
}
