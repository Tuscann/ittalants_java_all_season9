package Lecture_17.Hero_Woods.locations;

import Lecture_17.Hero_Woods.Creep;

import java.util.Random;

public class Woods {
    private final Creep[] creeps;

    public Woods() {
        this.creeps = new Creep[100];
        for (int i = 0; i < creeps.length; i++) {
            creeps[i] = new Creep("Gadina " + (i + 1));
        }
    }

    public Creep getRandomCreep() {
        Random r = new Random();
        int randomIndex = r.nextInt(creeps.length);
        return this.creeps[randomIndex];
    }
}
