package Lecture_12;

import java.util.Scanner;

public class _05_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a first string : ");
        String firstString = scan.nextLine();
        System.out.println("Write a second string : ");
        String secondString = scan.nextLine();

        char[] alphabet = new char[52];
        findCountLettersInWord(firstString, alphabet);
        char[] alphabet1 = new char[52];
        findCountLettersInWord(secondString, alphabet1);

        StringBuilder first = new StringBuilder();
        filingStringBulder(alphabet, first);
        StringBuilder second = new StringBuilder();
        filingStringBulder(alphabet1, second);

        if (first.toString().equals(second.toString())) {
            System.out.println("Same chars");
        } else {
            System.out.println("Diferent chars");
        }
    }

    private static void filingStringBulder(char[] alphabet, StringBuilder first) {
        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] != 0) {
                if (i < 26) {
                    for (int j = 0; j < alphabet[i]; j++) {
                        first.append((char) (i + 97));
                    }
                } else {
                    for (int j = 0; j < alphabet[i]; j++) {
                        first.append((char) (i + 39));
                    }
                }
            }
        }
    }

    private static void findCountLettersInWord(String word, char[] alphabet) {

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) >= 'a' && word.charAt(i) <= 'z') {
                alphabet[word.charAt(i) - 97]++;
            }
            if (word.charAt(i) >= 'A' && word.charAt(i) <= 'Z') {
                alphabet[word.charAt(i) - 39]++;
            }
        }
    }
}
