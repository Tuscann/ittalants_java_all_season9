package Lecture_12;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a string");
        String firstWord = scan.nextLine();

        String reverse = new StringBuffer(firstWord).reverse().toString();

        if (reverse.equals(firstWord)) {
            System.out.println("The word is palindrome");
        } else {
            System.out.println("The word is not palindrome");
        }
    }
}
