package Lecture_12;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String []input = scan.nextLine().split("");

        StringBuilder string = new StringBuilder();

        for (int i = 0; i < input.length; i++) {
            if (input[i].equals("2")){
                string.append("ABC");
            }
            else  if (input[i].equals("3")){
                string.append("DEF");
            }
            else  if (input[i].equals("4")){
                string.append("GHI");
            }
            else  if (input[i].equals("5")){
                string.append("JKL");
            }
            else  if (input[i].equals("6")){
                string.append("MNO");
            }
            else  if (input[i].equals("7")){
                string.append("PQRS");
            }
            else  if (input[i].equals("8")){
                string.append("TUV");
            }
            else  if (input[i].equals("9")){
                string.append("WXYZ");
            }
        }
        System.out.println(string.toString());


    }
}
