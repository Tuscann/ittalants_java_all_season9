package Lecture_12;

import java.util.Random;
import java.util.Scanner;

public class skalichka_bushesh_human_rock {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter difficulty (1 = easy, 2 = normal, 3 = hard, more = godMode)");
        int diff = sc.nextInt();
        sc.nextLine();
        while (true) {
            startGame(diff);
            System.out.println("Wanna play again? (enter 'no' for exit, enter 'yes' for new game)");
            String response = sc.nextLine();
            if (response.equals("no")) {
                System.out.println("Okey, pochivchica!");
                break;
            }
            else if(response.equals("yes")){
                startGame(diff);
            }
        }
    }

    static char askForDirection() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter direction (w = up, s = down, a = left, d = right)");
        char direction = sc.nextLine().charAt(0);
        return direction;
    }

    static void startGame(int diff) {
        char[][] world = generateWorld(diff);
        //place human and rock
        char human = 9786;
        char rock = 9679;
        int humanI = 3;
        int humanJ = 3;
        int rockI = 3;
        int rockJ = 4;
        world[humanI][humanJ] = human;
        world[rockI][rockJ] = rock;
        while (true) {
            printWorld(world);
            if (gameIsWon(world, rockI, rockJ)) {
                System.out.println("Well done!");
                break;
            }
            if (rockIsStuck(world, rockI, rockJ)) {
                System.out.println("Rock is stuck! Game over!");
                break;
            }
            char direction = askForDirection();
            switch (direction) {
                case 'w'://up
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanI - 1][humanJ] == '+' || world[humanI - 1][humanJ] == '#') {
                        continue;
                    }
                    if (world[humanI - 1][humanJ] == rock) {
                        if (world[humanI - 2][humanJ] == '+' || world[humanI - 2][humanJ] == '#') {
                            continue;
                        } else {
                            world[humanI - 2][humanJ] = rock;
                            rockI = humanI - 2;
                            rockJ = humanJ;
                        }
                    }
                    world[humanI - 1][humanJ] = human;
                    world[humanI][humanJ] = ' ';
                    humanI--;
                    break;
                case 's'://down
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanI + 1][humanJ] == '+' || world[humanI + 1][humanJ] == '#') {
                        continue;
                    }
                    if (world[humanI + 1][humanJ] == rock) {
                        if (world[humanI + 2][humanJ] == '+' || world[humanI + 2][humanJ] == '#') {
                            continue;
                        } else {
                            world[humanI + 2][humanJ] = rock;
                            rockI = humanI + 2;
                            rockJ = humanJ;
                        }
                    }
                    world[humanI + 1][humanJ] = human;
                    world[humanI][humanJ] = ' ';
                    humanI++;
                    break;
                case 'a'://left
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanI][humanJ - 1] == '+' || world[humanI][humanJ - 1] == '#') {
                        continue;
                    }
                    if (world[humanI][humanJ - 1] == rock) {
                        if (world[humanI][humanJ - 2] == '+' || world[humanI][humanJ - 2] == '#') {
                            continue;
                        } else {
                            world[humanI][humanJ - 2] = rock;
                            rockI = humanI;
                            rockJ = humanJ - 2;
                        }
                    }
                    world[humanI][humanJ - 1] = human;
                    world[humanI][humanJ] = ' ';
                    humanJ--;
                    break;
                case 'd'://right
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanI][humanJ + 1] == '+' || world[humanI][humanJ + 1] == '#') {
                        continue;
                    }
                    if (world[humanI][humanJ + 1] == rock) {
                        if (world[humanI][humanJ + 2] == '+' || world[humanI][humanJ + 2] == '#') {
                            continue;
                        } else {
                            world[humanI][humanJ + 2] = rock;
                            rockI = humanI;
                            rockJ = humanJ + 2;
                        }
                    }
                    world[humanI][humanJ + 1] = human;
                    world[humanI][humanJ] = ' ';
                    humanJ++;
                    break;
                default:
                    break;
            }
        }
    }

    static boolean rockIsStuck(char[][] world, int i, int j) {
        if (world[i][j - 1] == '+') {
            return true;
        }
        if (world[i - 1][j] == '+') {
            return true;
        }
        if (world[i][j + 1] == '+' && (world[i + 1][j] == '#' || world[i - 1][j] == '#')) {
            return true;
        }
        if (world[i + 1][j] == '+' && (world[i][j + 1] == '#' || world[i][j - 1] == '#')) {
            return true;
        }
        if (world[i][j - 1] == '#' && world[i - 1][j] == '#') {
            return true;
        }
        if (world[i][j + 1] == '#' && world[i - 1][j] == '#') {
            return true;
        }
        if (world[i][j - 1] == '#' && world[i + 1][j] == '#') {
            return true;
        }
        if (world[i][j + 1] == '#' && world[i + 1][j] == '#') {
            return true;
        }
        return false;
    }

    private static boolean gameIsWon(char[][] world, int rockI, int rockJ) {
        return rockI == world.length - 2 && rockJ == world.length - 2;
    }

    private static void printWorld(char[][] world) {
        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[i].length; j++) {
                System.out.print(world[i][j]);
            }
            System.out.println();
        }
    }

    private static char[][] generateWorld(int diff) {
        int dimension = 15;
        int bushes = diff * 10;
        char exit = 'X';
        char[][] world = new char[dimension + 2][dimension + 2];
        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[i].length; j++) {
                world[i][j] = ' ';
                if (i == 0 || i == world.length - 1) {
                    world[i][j] = '+';
                }
                if (j == 0 || j == world[i].length - 1) {
                    world[i][j] = '+';
                }
            }
        }
        Random r = new Random();

        for (int i = 0; i < bushes; i++) {
            int randI = r.nextInt(dimension) + 1;
            int randJ = r.nextInt(dimension) + 1;
            if (randI == dimension && randJ == dimension) {
                i--;
                continue;
            }
            world[randI][randJ] = '#';
        }
        //hoho haha
        world[dimension][dimension - 1] = ' ';
        world[dimension][dimension - 2] = ' ';
        world[dimension - 1][dimension] = ' ';
        world[dimension - 2][dimension] = ' ';
        world[dimension][dimension] = exit;
        if (rockIsStuck(world, 3, 4)) {
            return generateWorld(diff);
        }
        return world;
    }
}
