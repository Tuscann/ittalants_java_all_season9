package Lecture_12;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter word:");
        String word = sc.nextLine();

        char[] alphabet = new char[52];
        findCountLettersInWord(word, alphabet);
        printCountLettersInWord(alphabet);
    }
    static void findCountLettersInWord(String word, char[] alphabet) {

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) >= 'a' && word.charAt(i) <= 'z') {
                alphabet[word.charAt(i) - 97]++;
            }
            if (word.charAt(i) >= 'A' && word.charAt(i) <= 'Z') {
                alphabet[word.charAt(i) - 39]++;
            }
        }
    }

    static void printCountLettersInWord (char[] alphabet) {

        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] != 0) {
                if (i < 26) {
                    System.out.println((char) (i + 97) + " -> " + (int) alphabet[i]);
                }
                else {
                    System.out.println((char) (i + 39) + " -> " + (int) alphabet[i]);
                }
            }
        }
    }
}
