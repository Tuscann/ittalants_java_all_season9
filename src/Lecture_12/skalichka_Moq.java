package Lecture_12;

import java.util.Random;
import java.util.Scanner;


public class skalichka_Moq {
    public static void main(String[] args) {

        int dimension = 10;
        char[][] world = new char[dimension + 2][dimension + 2];
        CreateWorld(world, dimension);

        int humanX = 4;
        int humanY = 4;
        int rockX = humanX;
        int rockY = humanY + 1;
        char human = 9786;
        char rock = 9679;

        createBushes(world);
        createPerson_and_Rock(world, humanX, humanY, rockX, rockY);


        while (true) {
            printWorld(world);
            if (gameIsWon(world, rockX, rockY)) {
                System.out.println("Well done!");
                break;
            }
            if (rockIsStucks(world, rockX, rockY)) {
                System.out.println("Rock is stuck! Game over!");
                break;
            }
            char direction = giveDirection();
            switch (direction) {
                case 'w':  //left
                    if (world[humanX - 1][humanY] == '+' || world[humanX - 1][humanY] == '#') {
                        continue;
                    }
                    if (world[humanX - 1][humanY] == rock) {
                        if (world[humanX - 2][humanY] == '+' || world[humanX - 2][humanY] == '#') {
                            continue;
                        } else {
                            world[humanX - 2][humanY] = rock;
                            rockX = humanX - 2;
                            rockY = humanY;
                        }
                    }
                    world[humanX - 1][humanY] = human;
                    world[humanX][humanY] = ' ';
                    humanX--;
                    break;

                case 's'://down
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanX + 1][humanY] == '+' || world[humanX + 1][humanY] == '#') {
                        continue;
                    }
                    if (world[humanX + 1][humanY] == rock) {
                        if (world[humanX + 2][humanY] == '+' || world[humanX + 2][humanY] == '#') {
                            continue;
                        } else {
                            world[humanX + 2][humanY] = rock;
                            rockX = humanX + 2;
                            rockY = humanY;
                        }
                    }
                    world[humanX + 1][humanY] = human;
                    world[humanX][humanY] = ' ';
                    humanX++;
                    break;
                case 'a'://left
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanX][humanY - 1] == '+' || world[humanX][humanY - 1] == '#') {
                        continue;
                    }
                    if (world[humanX][humanY - 1] == rock) {
                        if (world[humanX][humanY - 2] == '+' || world[humanX][humanY - 2] == '#') {
                            continue;
                        } else {
                            world[humanX][humanY - 2] = rock;
                            rockX = humanX;
                            rockY = humanY - 2;
                        }
                    }
                    world[humanX][humanY - 1] = human;
                    world[humanX][humanY] = ' ';
                    humanY--;
                    break;
                case 'd'://right
                    //move human and move rock, but check if there is bush or wall
                    if (world[humanX][humanY + 1] == '+' || world[humanX][humanY + 1] == '#') {
                        continue;
                    }
                    if (world[humanX][humanY + 1] == rock) {
                        if (world[humanX][humanY + 2] == '+' || world[humanX][humanY + 2] == '#') {
                            continue;
                        } else {
                            world[humanX][humanY + 2] = rock;
                            rockX = humanX;
                            rockY = humanY + 2;
                        }
                    }
                    world[humanX][humanY + 1] = human;
                    world[humanX][humanY] = ' ';
                    humanY++;
                    break;
                default:
                    break;
            }

        }
    }

    private static boolean rockIsStucks(char[][] world, int i, int j) {
        if (world[i][j - 1] == '+') {
            return true;
        }
        if (world[i - 1][j] == '+') {
            return true;
        }
        if (world[i][j + 1] == '+' && (world[i + 1][j] == '#' || world[i - 1][j] == '#')) {
            return true;
        }
        if (world[i + 1][j] == '+' && (world[i][j + 1] == '#' || world[i][j - 1] == '#')) {
            return true;
        }
        if (world[i][j - 1] == '#' && world[i - 1][j] == '#') {
            return true;
        }
        if (world[i][j + 1] == '#' && world[i - 1][j] == '#') {
            return true;
        }
        if (world[i][j - 1] == '#' && world[i + 1][j] == '#') {
            return true;
        }
        if (world[i][j + 1] == '#' && world[i + 1][j] == '#') {
            return true;
        }
        return false;
    }

    private static boolean gameIsWon(char[][] world, int rockX, int rockY) {
        return rockX == world.length - 2 && rockY == world.length - 2;
    }

    private static char giveDirection() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter direction (w = up, s = down, a = left, d = right)");
        char direction = sc.nextLine().charAt(0);
        return direction;
    }

    private static void createPerson_and_Rock(char[][] world, int humanX, int humanY, int rockX, int rockY) {

        world[humanX][humanY] = 42;
        world[rockX][rockY] = 178;
    }

    private static void createBushes(char[][] world) {
        Scanner scan = new Scanner(System.in);
        System.out.println("0 = easy , 1 = medium , 2 = difficult , 3 == insane ,4 = goodMode");
        int diff = scan.nextInt();

        Random rand = new Random();
        for (int i = 0; i < (diff + 1) * 5; i++) {
            int curentBushX = rand.nextInt(world.length - 2) + 1;
            int curentBushY = rand.nextInt(world.length - 2) + 1;

            world[curentBushX][curentBushY] = 'x';
        }


    }

    private static void printWorld(char[][] world) {
        for (int row = 0; row < world.length; row++) {
            for (int col = 0; col < world[0].length; col++) {
                System.out.print(world[row][col]);
            }
            System.out.println();
        }
    }

    private static void CreateWorld(char[][] world, int dimension) {
        int exitI = dimension;
        int exitY = dimension;
        for (int row = 0; row < world.length; row++) {
            for (int col = 0; col < world[0].length; col++) {
                if (row == 0 || col == 0 || row == world.length - 1 || col == world.length - 1) {
                    world[row][col] = '+';
                } else {
                    world[row][col] = ' ';
                }
            }
        }

        world[dimension][dimension - 1] = ' ';
        world[dimension][dimension - 2] = ' ';
        world[dimension - 1][dimension] = ' ';
        world[dimension - 2][dimension] = ' ';
        world[dimension][dimension] = '0';
    }
}
