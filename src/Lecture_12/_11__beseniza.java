package Lecture_12;

import java.util.Random;
import java.util.Scanner;

public class _11__beseniza {
    public static void main(String[] args) {

        startGame();
    }

    private static void printSequence(char[] sec){
        for (char aSec : sec) {
            System.out.print(aSec);
        }
        System.out.println();
    }

    private static String getWordFromList(){
        String states = "Alabama,AL Alaska,AK Arizona,AZ Arkansas,AR California,CA Colorado,CO Connecticut,CT Delaware,DE District of Columbia,DC Florida,FL Georgia,GA Hawaii,HI Idaho,ID Illinois,IL Indiana,IN Iowa,IA Kansas,KS Kentucky,KY Louisiana,LA Maine,ME Montana,MT Nebraska,NE Nevada,NV New Hampshire,NH New Jersey,NJ New Mexico,NM New York,NY North Carolina,NC North Dakota,ND Ohio,OH Oklahoma,OK Oregon,OR Maryland,MD Massachusetts,MA Michigan,MI Minnesota,MN Mississippi,MS Missouri,MO Pennsylvania,PA Rhode Island,RI South Carolina,SC South Dakota,SD Tennessee,TN Texas,TX Utah,UT Vermont,VT Virginia,VA Washington,WA West Virginia,WV Wisconsin,WI Wyoming,WY ";
        states = states.replaceAll("[A-Z]{2}[ ]", "");
        states = states.substring(0, states.length()-1);
        String[] words = states.split(",");
        Random r = new Random();
        return words[r.nextInt(words.length)];
    }

    private static char[] createSequence(String word){
        char[] sequence = new char[word.length()];
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == ' '){
                sequence[i] = ' ';
            }
            else{
                sequence[i] = '_';
            }
        }
        return sequence;
    }

    private static char askForGuess(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Make a guess : ");
        return sc.nextLine().charAt(0);
    }

    private static void startGame(){
        //generate random word from some list of words
        String word = getWordFromList();
        word = word.toLowerCase();
        int maxErrors = 7;
        int errors = 0;
        //create a sequence of _____ for this word
        char[] sequence = createSequence(word);
        //ask for a guess
        while(true){
            printHangMan(errors);
            System.out.println("Errors left = " + (maxErrors - errors));
            System.out.println("Errors made = " + errors);
            printSequence(sequence);
            //check if word is guessed -> game over
            if(wordGuessed(sequence)){
                System.out.println("Well done!");
                break;
            }
            //if errors == maxErrors -> game over
            if(errors == maxErrors){
                System.out.println("Game over! You failed! The word was " + word);
                break;
            }
            char guess = askForGuess();
            //check the word for guessed char and apply on sequeence ___a__a_
            boolean missed = true;
            for (int i = 0; i < word.length(); i++) {
                if(word.charAt(i) == guess){
                    sequence[i] = guess;
                    missed = false;
                }
            }
            //check if guess is missed -> errors++, print
            if(missed){
                errors++;
            }
        }
    }

    private static void printHangMan(int errors) {
        char[][] hang0 = new char[0][0];
        char[][] hang1 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang2 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang3 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang4 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ','\\','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang5 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ','\\','|','/',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang6 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ','\\','|','/',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ','/',' ',' ',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][] hang7 = {
                {' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
                {' ',' ',' ','-','-','-','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','O',' ',' ','|',' ',' ',' '},
                {' ',' ','\\','|','/',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ',' ','|',' ',' ','|',' ',' ',' '},
                {' ',' ','/',' ','\\',' ','|',' ',' ',' '},
                {' ',' ',' ',' ',' ',' ','|',' ',' ',' '},
        };
        char[][][] kartinki = {hang0, hang1, hang2, hang3, hang4, hang5, hang6, hang7};
        char[][] kartinka = kartinki[errors];
        for (int i = 0; i < kartinka.length; i++) {
            for (int j = 0; j < kartinka[i].length; j++) {
                System.out.print(kartinka[i][j]);
            }
            System.out.println();
        }
    }

    static boolean wordGuessed(char[] sequence) {
        for (int i = 0; i < sequence.length; i++) {
            if(sequence[i] == '_'){
                return false;
            }
        }
        return true;
    }
}
