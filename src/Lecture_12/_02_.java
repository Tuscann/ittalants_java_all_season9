package Lecture_12;

import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write full name with spaces between : ");
        String[] name = scan.nextLine().split(" ");

        for (int i = 0; i < name.length; i++) {
            System.out.print(name[i].charAt(0)+" ");
        }
    }
}
