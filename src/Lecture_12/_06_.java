package Lecture_12;

import java.util.Scanner;

public class _06_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write s string : ");

        String[] input = scan.nextLine().split("");

        StringBuilder word = new StringBuilder();

        for (String anInput : input) {
            word.append(anInput);
        }
        System.out.println(word.toString());
    }
}
