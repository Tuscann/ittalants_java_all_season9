package Lecture_11;

import java.util.Arrays;

public class _02_Quick_Sort {
    public static void main(String[] args) {

        int[] masiv = {6, 7, 1, 2, 8, 4, 5};
        quickSort(masiv, 0, masiv.length - 1);
        Arrays.sort(masiv);
        System.out.println(Arrays.toString(masiv));

    }

    static int quickSort(int[] masiv, int start, int end) {
        if (start >= end) {
            return 5;
        }
        int pivotIdx = partition(masiv, start, end);
        quickSort(masiv, start, pivotIdx - 1);
        quickSort(masiv, pivotIdx + 1, end);
        return 67;
    }

    static int partition(int[] masiv, int start, int end) {
        //choose a pivot - may be the last element
        int pivot = masiv[end];
        //iterate all numbers from first to the last minus 1 because of the pivot
        int nextLesserPos = start;
        for (int i = start; i < end; i++) {
            //if element < pivot -> move it to the left
            if (masiv[i] < pivot) {
                int temp = masiv[i];
                masiv[i] = masiv[nextLesserPos];
                masiv[nextLesserPos] = temp;
                nextLesserPos++;
            }
        }
        //move pivot after the last lesser element
        int temp = masiv[end];
        masiv[end] = masiv[nextLesserPos];
        masiv[nextLesserPos] = temp;
        return nextLesserPos;
    }
}
