package Lecture_11;

public class _01_Binary_Search {
    public static void main(String[] args) {
        // fastest way to search on order array
        int[] arraySorted = new int[]{4, 5, 78, 129, 456, 789, 989, 7890, 98900};

        int x = 5;
        int index = findX(arraySorted, x, 0, arraySorted.length - 1);
        System.out.println(index);
    }

    static int findX(int[] masiv, int x, int start, int end) {
        // find mid element
        int mid = (start + end) / 2;
        //check if mid element == x --> return of mid element
        if (masiv[mid] == x) {
            return mid;
        }
        //check if mid element > x --> return of first(left) half
        else if (masiv[mid] > x) {
            return findX(masiv, x, start, mid - 1);
        }
        //check if mid element > x --> return of second(right) half
        else {
            return findX(masiv, x, mid + 1, end);
        }
    }
}

