package Lecture_09;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a integer");
        int x = scan.nextInt();

        int sum = sum(x);
        System.out.println(sum);


    }

    private static int sum(int x) {

        if (x == 1) {
            return 1;
        } else {
            return x + sum(x - 1);
        }

    }
}
