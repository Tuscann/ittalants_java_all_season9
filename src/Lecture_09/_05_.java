package Lecture_09;

public class _05_ {
    public static void main(String[] args) {
        char[][] room = {
                {'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S'},
                {'S', ' ', ' ', 'S', ' ', ' ', ' ', ' ', 'S', ' ', ' ', 'S'},
                {'S', ' ', 'X', 'S', ' ', ' ', ' ', ' ', 'S', ' ', 'X', 'S'},
                {'S', ' ', ' ', 'S', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'S'},
                {'S', 'S', 'S', 'S', ' ', ' ', ' ', ' ', 'S', ' ', ' ', 'S'},
                {'S', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'S', 'S', 'S', 'S'},
                {'S', ' ', ' ', ' ', 'S', 'S', 'S', ' ', ' ', ' ', ' ', 'S'},
                {'S', ' ', ' ', ' ', 'S', ' ', 'S', ' ', ' ', ' ', ' ', 'S'},
                {'S', ' ', ' ', ' ', 'S', ' ', 'S', 'S', ' ', ' ', ' ', 'S'},
                {'S', ' ', ' ', ' ', 'S', ' ', ' ', 'S', ' ', ' ', ' ', 'S'},
                {'S', ' ', ' ', ' ', 'S', ' ', ' ', 'S', ' ', ' ', ' ', 'S'},
                {'S', 'S', 'S', 'S', 'S', ' ', ' ', 'S', 'S', 'S', 'S', 'S'},

        };
        System.out.println(room[0].length);
        //print room
        printRoom(room);
        //gas room
        gasRoom(room, 2, 11);
        //print room
        printRoom(room);
    }

    private static void gasRoom(char[][] room, int row, int col) {
        if (!(room[row][col + 1] == 'S' || room[row][col + 1] == 'g')) { //right
            room[row][col + 1] = 'g';
            gasRoom(room, row, col + 1);
        }
        if (!(room[row + 1][col] == 'S' || room[row + 1][col] == 'g')) {//down
            room[row + 1][col] = 'g';
            gasRoom(room, row + 1, col);
        }
        if (!(room[row][col - 1] == 'S' || room[row][col - 1] == 'g')) { //left
            room[row][col - 1] = 'g';
            gasRoom(room, row, col - 1);
        }
        if (room[row - 1][col] != 'S' && room[row - 1][col] != 'g') { //up
            room[row - 1][col] = 'g';
            gasRoom(room, row - 1, col);
        }
    }

    private static void printRoom(char[][] room) {

        for (int row = 0; row < room.length; row++) {
            for (int col = 0; col < room[0].length; col++) {
                System.out.print(room[row][col]);
            }
            System.out.println();
        }
    }
}
