package Lecture_09;

import java.util.Arrays;

public class _01_ {
    public static void main(String[] args) {
        int[] array = {3, 7};

        System.out.print("Before " + Arrays.toString(array));

        swap(array);
        System.out.println("After" + Arrays.toString(array));
    }

    private static void swap(int[] array) {
        int temp = array[0];
        array[0] = array[1];
        array[1] = temp;
    }
}
