package Lecture_09;

public class _03_ {
    public static void main(String[] args) {
        int[] arr = {23, 42, 2, 3, 6, 3, 5, 36, 4, 4, 7, 57, 3, 5};

        int diff = findDiff(arr, 1, arr[0], arr[0]);
        System.out.println(diff);  
    }

    private static int findDiff(int[] arr, int i, int min, int max) {
        if (i == arr.length) {
            return max - min;
        }

        if (min > arr[i]) {
            min = arr[i];
        }

        if (max < arr[i]) {
            max = arr[i];
        }

        return findDiff(arr, i + 1, min, max);
    }
}
