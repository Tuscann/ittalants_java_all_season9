package Lecture_09;

import java.util.Arrays;

public class _02_ {
    public static void main(String[] args) {
        int[] array = {3, 7};

        System.out.println("Before " + Arrays.toString(array));

        swap(array);
        System.out.println("After" + Arrays.toString(array));
    }

    private static void swap(int[] array) {
        array = new int[5];
        System.out.println("Middle" + Arrays.toString(array));
    }
}
