package Lecture_15.Mortal_Combat;

import java.util.Random;

public class Fighter {
    String name;

    int damage = 10;
    Fighter enemy;
    int maxHealth = 390;
    int health = maxHealth;
    int evasionChance = 33;

    void hit() {
        System.out.println(name + " hits " + enemy.name + " for " + damage + " damage. ");
        if (!enemy.evade()) {
            enemy.health -= damage;
            enemy.heal();
        } else {
            System.out.println(enemy.name + " EVADES!!!!!!!!!!");
        }
        System.out.println(enemy.name + " is left with " + enemy.health + " heal!");
    }

    private void heal() {
        Random r = new Random();
        if (r.nextBoolean()) {
            System.out.println(name + " nameri i podushi MARKER !! SHIFF SHIFF");
            health += 6;
        }
    }

    private boolean evade() {
        Random r = new Random();
        return r.nextInt(99) + 1 <= evasionChance;
    }


    public boolean isAlive() {
        if (health > 0) {
            return true;
        }
        return false;
    }

    void resurect() {
        health = maxHealth;
    }
}
