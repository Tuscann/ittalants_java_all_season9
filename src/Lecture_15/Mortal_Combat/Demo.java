package Lecture_15.Mortal_Combat;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Fighter yoanita = new Fighter();
        yoanita.name = "Yonita";
        Fighter dido = new Fighter();
        dido.name = "Didonatora";

        yoanita.enemy = dido;
        dido.enemy = yoanita;

        int yoanaWins = 0;
        int didoWins = 0;

        yoanita.enemy = dido;
        dido.enemy = yoanita;
        yoanita.damage = 9;

        for (int i = 0; i < 1000; i++) {
            while (true) {
                Random r = new Random();
                if (r.nextBoolean()) {
                    dido.hit();
                    if (yoanita.isAlive()) {
                        yoanita.hit();
                        if (!dido.isAlive()) {
                            System.out.println(yoanita.name + " WINS!");
                            yoanaWins++;
                            break;
                        }
                    } else {
                        System.out.println(dido.name + " Wins!");
                        didoWins++;
                        break;
                    }
                } else {
                    yoanita.hit();

                    if (dido.isAlive()) {
                        dido.hit();
                        if (!yoanita.isAlive()) {
                            System.out.println(dido.name + " WINS!");
                            didoWins++;
                            break;
                        }
                    } else {
                        System.out.println(yoanita.name + " Wins!");
                        yoanaWins++;
                        break;
                    }
                }
            }
            dido.resurect();
            yoanita.resurect();
        }
        System.out.printf("%s wins = %d\n", dido.name, didoWins);
        System.out.printf("%s wins = %d", yoanita.name, yoanaWins);
    }
}
