package Lecture_15._02_Call_and_GSM;

public class GSM {
    private String model;
    private boolean hasSimCard;
    private String simMobileNumber = "null";
    private int outgoingCallsDuration;
    private String lastIncomingCall;
    private String lastOutgoingCall;

    public GSM(String model, boolean hasSimCard, String simMobileNumber, int outgoingCallsDuration, String lastIncomingCall, String lastOutgoingCall) {
        this.model = model;
        this.hasSimCard = hasSimCard;
        this.simMobileNumber = simMobileNumber;
        this.outgoingCallsDuration = outgoingCallsDuration;
        this.lastIncomingCall = lastIncomingCall;
        this.lastOutgoingCall = lastOutgoingCall;
    }

    public GSM(String model, boolean hasSimCard, int outgoingCallsDuration, String lastIncomingCall, String lastOutgoingCall) {
        this.model = model;
        this.hasSimCard = hasSimCard;
        this.outgoingCallsDuration = outgoingCallsDuration;
        this.lastIncomingCall = lastIncomingCall;
        this.lastOutgoingCall = lastOutgoingCall;
    }

    public void removeSimCard() {
        this.hasSimCard = false;
    }

    public void insertSimCard(String nomer) {
        if (nomer.length() == 10 && nomer.startsWith("08")) {
            this.hasSimCard = true;
            this.simMobileNumber = nomer;
        }
    }

    public void printInfoForTheLastOutgoingCall() {
        System.out.println("This ia last OutGoing call = " + this.lastOutgoingCall);
    }
    public void printInfoForTheLastIncomingCall() {
        System.out.println("This ia last Incoming call = " + this.lastIncomingCall);
    }

    public void call(String receiver, int duration) {
        Call first = new Call(0.2, "08974123123", "08974123456", 23);
    }

    @Override
    public String toString() {
        return "Model : " + this.model + "\nSimCard in : " + this.hasSimCard + "\nSimCard number = " + this.simMobileNumber + "\nOutgoing calls Duration : " + this.outgoingCallsDuration + "\nLast Income call : " + lastIncomingCall + "\nLastOutgoingCall : " + lastOutgoingCall;
    }

}
