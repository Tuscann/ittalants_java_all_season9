package Lecture_15._02_Call_and_GSM;

public class Main {
    public static void main(String[] args) {

        GSM firstGSM = new GSM("nokia101", true, "+359887383392", 45, "+359887111111", "+35988710000");
        GSM secondGSM = new GSM("Goggle Pixel 3", false, 12, "+359887123456", "+359887987654");
        System.out.println(firstGSM);
        System.out.println();
        System.out.println(secondGSM);
        System.out.println();
        firstGSM.removeSimCard();
        secondGSM.insertSimCard("0812456789");

        System.out.println(firstGSM);
        System.out.println();
        System.out.println(secondGSM);

       firstGSM.printInfoForTheLastIncomingCall();
       firstGSM.printInfoForTheLastOutgoingCall();
    }
}
