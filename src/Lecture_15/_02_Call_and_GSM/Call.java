package Lecture_15._02_Call_and_GSM;

public class Call {
    private double priceForAMinute;
    private String caller;
    private String receiver;
    private int duration;


    public Call(double priceForAMinute, String caller, String receiver, int duration) {
        this.priceForAMinute = priceForAMinute;
        this.caller = caller;
        this.receiver = receiver;
        this.duration = duration;
    }


}
