package Lecture_15.Tic_Tac_Toe;

import java.util.Random;
import java.util.Scanner;

public class Tic_Tac_Toe {

    public static void main(String[] args) {
        int rows = 13;
        int cols = 13;
        Scanner scan = new Scanner(System.in);

        char[][] table = new char[rows][cols];


        createTable(table);
        System.out.println("Player 'X', enter your move (row[1-3] column[1-3]):");
        Random r = new Random();
        int startPosition = r.nextInt(9);
        String[] input = scan.nextLine().split(" ");
        int sum = Integer.parseInt(input[0]) - 1 + Integer.parseInt(input[1]) - 1;
        System.out.println(sum);

        //       fillRightCell(table, startPosition);
        fillRightCell(table, sum);


        print(table);

    }

    private static void print(char[][] table) {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[0].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println();
        }
    }

    private static void createTable(char[][] table) {

        for (int rows = 0; rows < table.length; rows++) {
            for (int cols = 0; cols < table[rows].length; cols++) {
                if (rows == 0 || cols == 0 || rows == table.length - 1 || cols == table.length - 1 || rows % 4 == 0 || cols % 4 == 0) {
                    table[rows][cols] = '+';
                } else {
                    table[rows][cols] = ' ';
                }
            }
        }

    }

    private static void fillRightCell(char[][] table, int startPosition) {

        if (startPosition == 0) {
            table[1][1] = 'X';
            table[1][2] = 'X';
            table[1][3] = 'X';
            table[2][1] = 'X';
            table[2][2] = 'X';
            table[2][3] = 'X';
            table[3][1] = 'X';
            table[3][2] = 'X';
            table[3][3] = 'X';
        } else if (startPosition == 1) {
            table[1][5] = 'X';
            table[1][6] = 'X';
            table[1][7] = 'X';
            table[2][5] = 'X';
            table[2][6] = 'X';
            table[2][7] = 'X';
            table[3][5] = 'X';
            table[3][6] = 'X';
            table[3][7] = 'X';
        } else if (startPosition == 2) {
            table[1][9] = 'X';
            table[1][10] = 'X';
            table[1][11] = 'X';
            table[2][9] = 'X';
            table[2][10] = 'X';
            table[2][11] = 'X';
            table[3][9] = 'X';
            table[3][10] = 'X';
            table[3][11] = 'X';
        } else if (startPosition == 3) {
            table[5][1] = 'X';
            table[6][1] = 'X';
            table[7][1] = 'X';
            table[5][2] = 'X';
            table[6][2] = 'X';
            table[7][2] = 'X';
            table[5][3] = 'X';
            table[6][3] = 'X';
            table[7][3] = 'X';
        } else if (startPosition == 4) {
            table[5][5] = 'X';
            table[6][5] = 'X';
            table[7][5] = 'X';
            table[5][6] = 'X';
            table[6][6] = 'X';
            table[7][6] = 'X';
            table[5][7] = 'X';
            table[6][7] = 'X';
            table[7][7] = 'X';
        } else if (startPosition == 5) {
            table[5][9] = 'X';
            table[6][9] = 'X';
            table[7][9] = 'X';
            table[5][10] = 'X';
            table[6][10] = 'X';
            table[7][10] = 'X';
            table[5][11] = 'X';
            table[6][11] = 'X';
            table[7][11] = 'X';
        } else if (startPosition == 6) {
            table[9][1] = 'X';
            table[10][1] = 'X';
            table[11][1] = 'X';
            table[9][2] = 'X';
            table[10][2] = 'X';
            table[11][2] = 'X';
            table[9][3] = 'X';
            table[10][3] = 'X';
            table[11][3] = 'X';
        } else if (startPosition == 7) {
            table[9][5] = 'X';
            table[10][5] = 'X';
            table[11][5] = 'X';
            table[9][6] = 'X';
            table[10][6] = 'X';
            table[11][6] = 'X';
            table[9][7] = 'X';
            table[10][7] = 'X';
            table[11][7] = 'X';
        } else if (startPosition == 8) {
            table[9][9] = 'X';
            table[10][9] = 'X';
            table[11][9] = 'X';
            table[9][10] = 'X';
            table[10][10] = 'X';
            table[11][10] = 'X';
            table[9][11] = 'X';
            table[10][11] = 'X';
            table[11][11] = 'X';
        }
    }
}
