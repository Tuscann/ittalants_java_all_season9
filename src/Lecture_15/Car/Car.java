package Lecture_15.Car;

public class Car {
    public int speed;
    public String color;
    public int gear;
    public double maxSpeed;



    public void changeGearUp() {
        if (gear < 5) {
            gear++;
        }
    }

    void accelerate(int speeeed) {
        if (speed <= maxSpeed) {
            speed = speeeed;
        }
    }

    void changeGearDown() {
        if (gear > 0) {
            gear--;
        } else {
            gear = 1;
            System.out.println("You are now on 1st gear!!!");
        }
    }

    void changeGear(int nextGear) {
        if (nextGear > 0 && nextGear < 6) {
            gear = nextGear;
        }
    }


    void changeColor(String newColor) {
        color = newColor;
    }

    @Override
    public String toString() {
        return "Speed : " + speed + "\nColor = " + color + "\nGear = " + gear + "\nMaxSpeed = " + maxSpeed;
    }
}
