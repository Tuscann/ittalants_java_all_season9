package Lecture_15.Car;

public class CarDemo {
    public static void main(String[] args) {
        Car golf = new Car();
        golf.speed = 100;
        golf.color = "Red";
        golf.gear = 0;
        golf.maxSpeed = 320.5;

        Car honda = new Car();
        honda.gear = 5;


        golf.changeGearDown();
        golf.changeGearUp();
        golf.changeGear(5);

        System.out.println(golf);
        System.out.println(honda);

        System.out.println("The current speed of the golf is = " + golf.speed);
        golf.accelerate(140);
        System.out.println("The current speed of the golf is = " + golf.speed);
        System.out.println("The current gear is " + golf.gear);
        for (int i = 0; i < 10; i++) {
            golf.changeGearUp();
        }
        System.out.println("The current gear is " + golf.gear);
        System.out.println("The Honda's current gear is " + honda.gear);
        honda.changeGear(1);
        System.out.println("The Honda's current gear is " + honda.gear);
        golf.changeColor("Blue");
        golf.changeColor("Red");
        System.out.println("Golf color = " + golf.color);

    }
}
