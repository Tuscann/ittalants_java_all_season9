package Lecture_15.Person;

public class Person {
    String name;
    int age;
    long personalNumber;
    boolean isWoman;
    Double weight;


    @Override
    public String toString() {
        return "Name : " + name + "\nAge = " + age + "\nPersonal Number = " + personalNumber + "\nIs Woman : " + isWoman + "\nWeight = " + weight;
    }
}
