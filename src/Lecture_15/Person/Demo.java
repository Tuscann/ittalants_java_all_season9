package Lecture_15.Person;

public class Demo {
    public static void main(String[] args) {
        Person ivan = new Person();
        Person maria = new Person();

        ivan.name = "Ivan";
        maria.name = "Maria";
        ivan.weight = 80.5;
        maria.weight = 60.5;
        ivan.age = 32;
        maria.age = 24;
        ivan.personalNumber = 349742346;
        maria.personalNumber = 346;
        


        System.out.println(ivan);
        System.out.println();
        System.out.println(maria);
    }
}
