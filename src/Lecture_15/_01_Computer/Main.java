package Lecture_15._01_Computer;

public class Main {
    public static void main(String[] args) {
        Computer firstComputer = new Computer(1989, 1400, false, 120, 7.2, "Linux");
        Computer notebook = new Computer(1960, 2012.45, true, 140.98, 16.60, "Windows 10");

//        System.out.println(firstComputer.toString());
//        System.out.println();
//        System.out.println(notebook.toString());

        firstComputer.changeOperationSystem("Windows 7");
        firstComputer.useMemory(12);

        System.out.println("After : ");
        System.out.println(firstComputer.toString());
        System.out.println();
        System.out.println(notebook.toString());
    }
}
