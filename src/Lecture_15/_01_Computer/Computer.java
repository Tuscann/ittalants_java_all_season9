package Lecture_15._01_Computer;

public class Computer {
    private int year;
    private double price;
    private boolean isNotebook;
    private double hardDiskMemory;
    private double freeMemory;
    private String operationSystem;

    public Computer(int year, double price, boolean isNotebook, double hardDiskMemory, double freeMemory, String operationSystem) {
        this.year = year;
        this.price = price;
        this.isNotebook = isNotebook;
        this.hardDiskMemory = hardDiskMemory;
        this.freeMemory = freeMemory;
        this.operationSystem = operationSystem;
    }

    @Override
    public String toString() {
        return "Building year = " + year + "\nPrice = " + price + "\nIsNotebook = " + isNotebook + "\nTotal hardDisk space = " + hardDiskMemory + "\nFreeMemory = " + freeMemory + "\nOperation System : " + operationSystem;
    }

    public void changeOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    public void useMemory(double memory) {
        if (this.freeMemory < memory) {
            System.out.println("Not enough free memory!");
        } else {
            this.freeMemory -= memory;
        }
    }
}
