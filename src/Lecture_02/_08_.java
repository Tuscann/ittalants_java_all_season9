package Lecture_02;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add number between 1000 and 9999 :");
        int number = scan.nextInt();

        if (number >= 1000 && number <= 9999) {
            int number1 = (number / 1000) * 10 + number % 10;
            int number2 = ((number / 100)%10)*10+(number / 10) % 10;

            if (number1 < number2) {
                System.out.printf("%d < %d", number1, number2);
            } else if (number1 == number2) {
                System.out.printf("%d == %d", number1, number2);
            } else if (number1 > number2) {
                System.out.printf("%d < %d", number2, number1);
            }
        }
    }
}
