package Lecture_02;

import java.util.Scanner;

public class _05_3_numbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Number A:");
        double numberA = scan.nextDouble();

        System.out.println("Number B:");
        double numberB = scan.nextDouble();

        System.out.println("Number C:");
        double numberC = scan.nextDouble();

        if (numberA != numberB && numberA != numberC && numberB != numberC) {
            System.out.print("The numbers in descending order are: \n");
            if (numberA > numberB && numberA > numberC) {
                if (numberB > numberC) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberA, numberB, numberC);
                } else if (numberB < numberC) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberA, numberC, numberB);
                }
            } else if (numberB > numberA && numberB > numberC) {
                if (numberA > numberC) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberB, numberA, numberC);
                } else if (numberA < numberC) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberB, numberC, numberA);
                }
            } else if (numberC > numberA && numberC > numberB) {
                if (numberA > numberB) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberC, numberA, numberB);
                } else if (numberA < numberB) {
                    System.out.printf("%.1f > %.1f > %.1f ", numberC, numberB, numberA);
                }
            }
        }
        else {
            System.out.println("Two numbers are equals.");
        }
    }
}
