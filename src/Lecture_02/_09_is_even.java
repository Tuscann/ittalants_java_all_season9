package Lecture_02;
import java.util.Scanner;

public class _09_is_even {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Number A:");
        int numberA = scan.nextInt();

        System.out.println("Number B:");
        int numberB = scan.nextInt();

        if (numberA >= 10 && numberA <= 100 && numberB >= 10 && numberB <= 100) {
            int product = numberA * numberB;
            int lastDigitOfProduct = product % 10;

            if (product % 10 != 1) {
                System.out.printf("%d, %d четна", product, lastDigitOfProduct);
            }
            else {
                System.out.printf("%d, %d нечетна", product,lastDigitOfProduct);
            }
        }
    }
}