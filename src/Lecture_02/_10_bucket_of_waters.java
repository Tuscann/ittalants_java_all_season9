package Lecture_02;
import java.util.Scanner;

public class _10_bucket_of_waters {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add total volume :");
        int totalWater = scan.nextInt();

        if (totalWater >= 10 && totalWater <= 9999) {
            int cources = totalWater / 5;
            int reminder = totalWater % 5;

            if (reminder == 0) {
                System.out.println(cources + " пъти по 3 литра,");
                System.out.println(cources + " пъти по 2 литра");
            } else if (reminder == 1) {
                System.out.println(cources - 1 + " пъти по 3 литра,");
                System.out.println(cources + 2 + " пъти по 2 литра");
            }
            else if (reminder == 2) {
                System.out.println(cources + " пъти по 3 литра,");
                System.out.println(cources + " пъти по 2 литра");
                System.out.println("допълнително кофа от 2 литра");
            }
            else if (reminder == 3) {
                System.out.println(cources + " пъти по 3 литра,");
                System.out.println(cources  + " пъти по 2 литра");
                System.out.println("допълнително кофа от 3 литра");
            }
            else if (reminder == 4) {
                System.out.println(cources + " пъти по 3 литра,");
                System.out.println(cources  + " пъти по 2 литра");
                System.out.println("допълнително 2 кофа от 2 литра");
            }
        }
    }
}
