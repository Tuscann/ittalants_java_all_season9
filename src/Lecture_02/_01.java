package Lecture_02;

import java.util.Scanner;

public class _01 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Въведете A:");
        double a = scan.nextDouble();
        System.out.println("Въведете B:");
        double b = scan.nextDouble();
        System.out.println("Въведете C:");
        double c = scan.nextDouble();

        if (a <= c && c <= b) {
            System.out.printf("Числото %.1f е между %.1f и %.1f ",c,a,b);
        }
        else {
            System.out.printf("Числото %.1f не е между %.1f и %.1f ",c,a,b);
        }
    }
}
