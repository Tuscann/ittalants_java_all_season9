package Lecture_02;

import java.util.Scanner;

public class _14_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Add four numbers between 1 and 8:");

        int positionOneX = sc.nextInt();
        int positionOneY = sc.nextInt();
        int positionTwoX = sc.nextInt();
        int positionTwoY = sc.nextInt();

        if (positionOneX >= 1 && positionOneX <= 8 && positionOneY >= 1 && positionOneY <= 8 && positionTwoX >= 1 && positionTwoX <= 8 && positionTwoY >= 1 && positionTwoY <= 8) {
            if (positionOneX == positionTwoX && positionOneY == positionTwoY) {
                System.out.println("Positions are with the same color.");
            } else {
                System.out.println("Positions are with different color.");
            }
        }
    }
}
