package Lecture_02;

import java.util.Scanner;

public class _02_with_doubles {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Въведете A:");
        double a = scan.nextDouble();

        System.out.println("Въведете B:");
        double b = scan.nextDouble();

        double sum = a + b;
        double minus = a - b;
        double product = a * b;
        double division = a / b;
        double division2 = a % b;

        System.out.printf("Сумата на %.1f + %.1f = %.1f\n", a, b, sum);
        System.out.printf("Разликата на %.1f - %.1f = %.1f\n", a, b, minus);
        System.out.printf("Произведение на %.1f * %.1f = %.1f\n", a, b, product);
        if (b == 0) {
            System.out.println("Няма такова число");
            System.out.println("Безкраиност");
        } else {
            System.out.println("Остатък на " + a + " % " + b + " = " + division2);
            System.out.printf("Целочислено деление на %.1f / %.1f = %.1f\n", a, b, division);
        }



    }
}
