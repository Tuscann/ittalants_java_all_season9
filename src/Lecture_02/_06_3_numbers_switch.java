package Lecture_02;

import java.util.Scanner;

public class _06_3_numbers_switch {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Number A:");
        double numberA = scan.nextDouble();

        System.out.println("Number B:");
        double numberB = scan.nextDouble();

        System.out.println("Number C:");
        double numberC = scan.nextDouble();

        System.out.printf("Value of a = %.1f\n", numberA);
        System.out.printf("Value of b = %.1f\n", numberB);
        System.out.printf("Value of c = %.1f\n", numberC);
        double numberD = numberA;
        numberA = numberB;
        numberB = numberC;
        numberC = numberD;
        System.out.println("After change of the values : ");
        System.out.printf("Value of a = %.1f\n", numberA);
        System.out.printf("Value of b = %.1f\n", numberB);
        System.out.printf("Value of c = %.1f\n", numberC);

    }
}
