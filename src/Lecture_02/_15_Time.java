package Lecture_02;

import java.util.Scanner;

public class _15_Time {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Add time : ");
        int n = scan.nextInt();

        if (n >= 0 && n <= 23) {
            if (n >= 4 && n <= 9) {
                System.out.println("Добро утро;");
            } else if (n > 9 && n <= 18) {
                System.out.println("Добър ден;");
            } else if (n > 18 || n < 4) {
                System.out.println("Добър вечер;");
            }
        }
    }
}
