package Lecture_02;

import java.util.Scanner;

public class _12_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter date : ");
        int day = scan.nextInt();
        System.out.print("Enter month : ");
        int month = scan.nextInt();
        System.out.print("Enter year : ");
        int year = scan.nextInt();

        System.out.printf("Start date : %d,%d,%d\n", day, month, year);
        System.out.print("End date   : ");

        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (day == 31) {
                day = 1;
                month += 1;
                if (month == 12) {
                    month = 1;
                    year += 1;
                }
            } else if (day > 0 && day < 31) {
                day += 1;
            }
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day == 30) {
                day = 1;
                month += 1;
            } else if (day > 0 && day < 30) {
                day += 1;
            }
        } else if (month == 2) {
            if (year % 4 == 0 && year % 100 != 0) {
                if (day == 29) {
                    day = 1;
                    month += 1;
                } else if (day > 0 && day < 29) {
                    day += 1;
                }
            } else {
                if (day == 28) {
                    day = 1;
                    month += 1;
                } else if (day > 0 && day < 28) {
                    day += 1;
                }
            }
        }
        System.out.println(day + "," + month + "," + year);
    }
}
