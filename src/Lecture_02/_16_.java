package Lecture_02;

import java.util.Scanner;

public class _16_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Add number between 100 and 999 : ");
        int number = sc.nextInt();

        int firstDigit = number / 100;
        int secondDigit = (number / 10) % 10;
        int thirdDigit = number % 10;

        if (number >= 100 && number <= 1000) {
            if (firstDigit == secondDigit && secondDigit == thirdDigit) {
                System.out.println("Equal.");
            } else if (firstDigit > secondDigit && secondDigit > thirdDigit) {
                System.out.println("Descending order");
            } else if (firstDigit < secondDigit && secondDigit < thirdDigit) {
                System.out.println("Ascending order");
            } else {
                System.out.println("Unordered");
            }
        }
    }
}
