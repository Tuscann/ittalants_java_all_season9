package Lecture_02;

import java.util.Scanner;

public class _11_3_numbers_withoout_zero {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write number between 100 and 999 : ");
        int number = scan.nextInt();

        if (number >= 100 && number <= 999) {
            int firstDigit = number / 100;
            int secondDigit = (number / 10) % 10;
            int thirdDigit = number % 10;

            if (secondDigit != 0 && thirdDigit != 0) {
                if (number % firstDigit == 0 && number % secondDigit == 0 && number % thirdDigit == 0) {
                    System.out.println("True");
                } else {
                    System.out.println("False");
                }
            } else {
                System.out.printf("%d contains zero digits.", number);
            }
        }
    }
}
