package Lecture_02;

import java.util.Scanner;

public class _07_Is_health {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Time : ");
        int time = scan.nextInt();

        System.out.println("Sum money : ");
        double money = scan.nextDouble();

        System.out.println("Is am ill ? (true || false)");
        boolean isHealth = scan.nextBoolean();

        if (isHealth) {
            System.out.println("Stay in my home.");
        } else {
            System.out.println("I will on cinema with friends.");
        }

        if (money > 0) {
            System.out.println("I will buy medicines.");
        } else if (money > 0 && money < 10.0) {
            System.out.println("I will go for coffee.");
        } else if (money == 0) {
            System.out.println("I will stay in my home and drink tea.");
        }
    }
}
