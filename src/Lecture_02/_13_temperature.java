package Lecture_02;

import java.util.Scanner;

public class _13_temperature {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Add temperature : ");
        int temperature = scan.nextInt();

        if (temperature >= -100 && temperature < -20) {
            System.out.println("Ice cold");
        } else if (temperature < 0) {
            System.out.println("Cold");
        } else if (temperature < 15) {
            System.out.println("Cool");
        } else if (temperature <= 25) {
            System.out.println("Warm");
        } else if (temperature <= 100) {
            System.out.println("Hot");
        }

    }
}
