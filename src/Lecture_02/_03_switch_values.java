package Lecture_02;

import java.util.Scanner;

public class _03_switch_values {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Въведете A:");
        double a = scan.nextDouble();

        System.out.println("Въведете B:");
        double b = scan.nextDouble();

        System.out.println("Value of a before switch = " + a);
        System.out.println("Value of a before switch = " + b);
//        double c = b;
//        b = a;
//        a = c;
        b = a + (a = b) - b;

        System.out.println("Value of a before switch = " + a);
        System.out.println("Value of a before switch = " + b);


    }
}
