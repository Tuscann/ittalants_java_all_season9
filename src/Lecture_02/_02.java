package Lecture_02;

import java.util.Scanner;

public class _02 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Въведете A:");
        int a = scan.nextInt();

        System.out.println("Въведете B:");
        int b = scan.nextInt();

        int sum = a + b;
        int minus = a - b;
        int product = a * b;
        int division = a / b;
        int division2 = a % b;

        System.out.printf("Сумата на %d + %d = %d\n", a, b, sum);
        System.out.printf("Разликата на %d - %d = %d\n", a, b, minus);
        System.out.printf("Произведение на %d * %d = %d\n", a, b, product);

        System.out.println("Остатък на " + a + " % " + b + " = " + division2);
        System.out.printf("Целочислено деление на %d / %d = %d\n", a, b, division);
    }
}
