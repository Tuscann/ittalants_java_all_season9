package Lecture_08;

import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string : ");
        String first = scan.nextLine();
        System.out.println("Write a string : ");
        String second = scan.nextLine();

        if (first.length() <= 40 && second.length() <= 40){
            System.out.print(first.toUpperCase()+" ");
            System.out.print(first.toLowerCase()+" ");
            System.out.print(second.toUpperCase()+" ");
            System.out.print(second.toLowerCase());
        }else {
            System.out.println("Length of string more than 40.");
        }
    }
}
