package Lecture_08;

import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string : ");
        String first = scan.nextLine();
        System.out.println("Write a string : ");
        String second = scan.nextLine();

        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        if (first.length() >= 10 && first.length() <= 20 && second.length() >= 10 && second.length() <= 20) {
            sb1.append(first);
            sb2.append(second);

            for (int i = 0; i < 5; i++) {
                sb1.setCharAt(i, second.charAt(i));
                sb2.setCharAt(i, first.charAt(i));
            }

            if (sb1.length() > sb2.length()) {
                System.out.println(sb1.length() + " " + sb1);
            } else if (sb1.length() < sb2.length()) {
                System.out.println(sb2.length() + " " + sb2);
            } else {
                System.out.println("Equals length " + sb2.length());
            }
        }
        else {
            System.out.println("Length of string need to be 10 >= x <= 20");
        }
    }
}
