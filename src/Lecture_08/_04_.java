package Lecture_08;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string split by , : ");
        String x = scan.nextLine();

        String[] animalsArray = x.trim().split(",");

        int sum1 = 0;
        int sum2 = 0;

        for (int i = 0; i < animalsArray[0].length(); i++) {
            sum1 += (int) animalsArray[0].charAt(i);
        }
        for (int i = 0; i < animalsArray[1].length(); i++) {
            sum2 += (int) animalsArray[1].charAt(i);
        }
        System.out.print("Longest string : ");

        if (sum1 >= sum2) {
            System.out.println(animalsArray[0].trim());
            System.out.print("Shortest string : ");
            System.out.println(animalsArray[1].trim());
        } else {
            System.out.println(animalsArray[1].trim());
            System.out.print("Shortest string : ");
            System.out.println(animalsArray[0].trim());
        }
    }
}
