package Lecture_08;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _09_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write mix integers and words");
        String text = scan.nextLine();
        long sum = 0;

        Pattern pattern = Pattern.compile("(-?)[0-9]+");
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            sum += Integer.parseInt(matcher.group());
        }

        System.out.println("The sum is: " + sum);

    }
}
