package Lecture_08;

import java.util.Scanner;

public class _09__ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write mix integers and words");
        char[] input = sc.nextLine().trim().toCharArray();

        int sum = 0;
        StringBuilder current = new StringBuilder();

        for (int i = 0; i < input.length; i++) {
            char currentChar = input[i];

            if (currentChar == '-') {
                if (current.toString().contains("-")) {
                    int number = Integer.parseInt(current.toString());
                    sum += number;
                    System.out.println(number);
                    current = new StringBuilder("-");
                } else if (!current.toString().contains("-") && !current.toString().equals("")) {
                    int number = Integer.parseInt(current.toString());
                    sum += number;
                    System.out.println(number);
                    current = new StringBuilder("-");
                } else {
                    current.append(currentChar);
                }

            } else if (Character.isDigit(currentChar)) {
                current.append(currentChar);
                if (i == input.length - 1) {
                    int number = Integer.parseInt(current.toString());
                    sum += number;
                    System.out.println(number);
                }
            } else {
                if (!current.toString().equals("")  ) {
                    if (!current.toString().equals("-")) {
                        int number = Integer.parseInt(current.toString());
                        sum += number;
                        System.out.println(number);
                    }
                }
                current = new StringBuilder();
            }
        }
        System.out.println("Sum = " + sum);
    }
}
