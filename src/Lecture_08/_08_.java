package Lecture_08;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write word ");

        String[] array = scan.nextLine().trim().split("");

        boolean isFound = true;
        for (int i = 0; i < array.length-1; i++) {
            if (!array[i].equals(array[array.length - i-1])) {
                isFound = false;
                break;
            }
        }
        if (isFound) {
            System.out.println("Word is palindrome");
        }
        else {
            System.out.println("Word is not palindrome");
        }
    }
}
