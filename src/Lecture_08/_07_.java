package Lecture_08;

import java.util.Scanner;

public class _07_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string split by space : ");
        String x = scan.nextLine();

        String[] array = x.trim().split(" ");

        int lenght = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() > lenght) {
                lenght = array[i].length();
            }
        }
        System.out.printf("%d words.\nLongest word is with %d chars.", array.length,lenght);
    }
}
