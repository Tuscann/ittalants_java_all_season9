package Lecture_08;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string : ");
        String first = scan.nextLine();
        System.out.println("Write a string : ");
        String second = scan.nextLine();

        if (first.length() < second.length()) {
            System.out.println("The length of the second String is bigger than length of the first");
        } else if (first.length() > second.length()) {
            System.out.println("The length of the first String is bigger than length of the second");
        } else {
            System.out.println("Two strings are with same length");

            for (int i = 0; i < second.length(); i++) {
                if (first.charAt(i) != second.charAt(i)) {
                    System.out.printf("%d %c-%c\n", i + 1, first.charAt(i), second.charAt(i));
                }
            }
        }
    }
}
