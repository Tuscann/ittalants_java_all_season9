package Lecture_08;

import java.util.Scanner;

public class _06_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string split by space : ");
        String x = scan.nextLine();

        String[] array = x.trim().split(" ");

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length(); j++) {
                if (j == 0) {
                    System.out.print(Character.toUpperCase(array[i].charAt(j)));

                } else {
                    System.out.print(array[i].charAt(j));
                }
            }
            System.out.print(" ");
        }

    }
}
