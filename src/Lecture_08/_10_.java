package Lecture_08;

import java.util.Scanner;

public class _10_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string");

        char[] ivan = scan.nextLine().toCharArray();

        System.out.print("String before : ");
        for (int i = 0; i < ivan.length; i++) {
            System.out.print(ivan[i]);
        }
        System.out.println();
        System.out.print("String  after : ");
        for (int i = 0; i < ivan.length; i++) {
            int currentChar = (int) ivan[i] + 5;
            System.out.print((char) currentChar );
        }
    }
}
