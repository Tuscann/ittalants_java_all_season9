package Lecture_08.InClass;

public class _03_ {
    public static void main(String[] args) {

        int[] left_array = {1, 3, -5, 4};
        int[] right_array = {1, 4, -5, -2};

        printArray(left_array,right_array);

    }

    private static void printArray(int[] left_array, int[] right_array) {
        System.out.print("Array 1 : ");
        for (int i = 0; i < left_array.length; i++) {
            System.out.print(left_array[i] + " ");
        }
        System.out.println();
        System.out.print("Array 2 : ");
        for (int i = 0; i < right_array.length; i++) {
            System.out.print(right_array[i] + " ");
        }
        System.out.println();
        System.out.print("Result Array: ");
        int[] array = new int[left_array.length];
        for (int i = 0; i < array.length; i++) {
            int num1 = left_array[i];
            int num2 = right_array[i];
            array[i] = num1 * num2;

            System.out.print(array[i] + " ");
        }
    }
}
