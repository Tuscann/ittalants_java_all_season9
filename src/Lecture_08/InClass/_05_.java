package Lecture_08.InClass;

public class _05_ {
    public static void main(String[] args) {
        String str2 = "MIV";
        String str = "MCMIV";
        String str3 = "MMI";

        System.out.printf("Integer form of Roman Numeral is %d\n" , ob(str));
        System.out.printf("Integer form of Roman Numeral is %d\n", ob(str2));
        System.out.printf("Integer form of Roman Numeral is %d\n" , ob(str3));
    }

    private static int ob(String str) {

        int res = 0;

        for (int i = 0; i < str.length(); i++) {
            int s1 = value(str.charAt(i)); // Getting value of symbol s[i]

            if (i + 1 < str.length()) {  // Getting value of symbol s[i+1]
                int s2 = value(str.charAt(i + 1));

                if (s1 >= s2) {  // Comparing both values
                    // Value of current symbol is greater
                    // or equalto the next symbol
                    res = res + s1;
                } else {
                    res = res + s2 - s1;
                    i++; // Value of current symbol is
                    // less than the next symbol
                }
            } else {
                res = res + s1;
                i++;
            }
        }

        return res;
    }

    static public int value(char r) {
        if (r == 'I') return 1;
        else if (r == 'V') return 5;
        else if (r == 'X') return 10;
        else if (r == 'L') return 50;
        else if (r == 'C') return 100;
        else if (r == 'D') return 500;
        else if (r == 'M') return 1000;
        return -1;
    }
}
