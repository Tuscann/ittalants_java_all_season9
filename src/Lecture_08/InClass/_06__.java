package Lecture_08.InClass;

import java.util.Scanner;

public class _06__ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number ");
        int n = scan.nextInt();

        System.out.println(factorial(n));
    }

    private static long factorial(int n) {
        if (n <= 1) return 1;
        else return n * factorial(n - 1);
    }
}
