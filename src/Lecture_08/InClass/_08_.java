package Lecture_08.InClass;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int x = scan.nextInt();
        System.out.println("Write a number : ");
        int z = scan.nextInt();

        System.out.printf("Simple fraction : %d/%d = %d/%d\n", x, z, x / CrossDevision(x, z), z / CrossDevision(x, z));

    }

    private static int CrossDevision(int a, int b) {
        if (b == 0) return a;
        return CrossDevision(b, a % b);
    }
}
