package Lecture_08.InClass;

public class _01_ {
    public static void main(String[] args) {

        int[] x = new int[]{
                1, 2, 3, 4, 5
        };
        printArray(x);
    }

    private static void printArray(int[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
    }
}
