package Lecture_08.InClass;

import java.util.Scanner;

public class _06_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number ");
        int n = scan.nextInt();

        System.out.println(factorel(n));
    }

    private static long factorel(int n) {
        long sum = 1;

        for (int i = 1; i <= n; i++) {
            sum *= i;
        }
        return sum;
    }
}
