package Lecture_08.InClass;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Find Fibonacci number");
        int n = scan.nextInt();

        findFibonachi(n);
    }

    private static void findFibonachi(int n) {
        int n1 = 0, n2 = 1, n3;

        for (int i = 2; i <= n; ++i)//loop starts from 2 because 0 and 1 are already printed
        {
            n3 = n1 + n2;
            if (i == n) {
                System.out.printf("Fibonacci %d = %d", n, n3);
            }
            n1 = n2;
            n2 = n3;
        }
    }


}
