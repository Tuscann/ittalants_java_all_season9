package Lecture_08.InClass;

import java.util.Random;

public class _02_ {
    public static void main(String[] args) {
        int[] x = new int[7];
        printArray(x);

    }

    private static void printArray(int[] x) {
        Random rand = new Random();

        for (int i = 0; i < x.length; i++) {
            x[i] = rand.nextInt(10) + 1;
            System.out.print(x[i] + " ");
        }
    }
}
