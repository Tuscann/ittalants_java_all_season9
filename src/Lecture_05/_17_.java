package Lecture_05;

import java.util.Scanner;

public class _17_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the length of the array:");
        int a = Integer.parseInt(scanner.nextLine());

        double[] array = new double[a];

        System.out.println("Fill the array:");

        for (int i = 0; i < array.length; i++) {
            System.out.print("Enter element number " + (i + 1) + " for the array : ");
            array[i] = Double.parseDouble(scanner.nextLine());
        }
        boolean isZigZagUpwards = true;

        for (int i = 0; i < array.length - 2; i+=2) {
            if (!((array[i] < array[i + 1] && array[i + 1] > array[i + 2])
                    || (array[i] > array[i + 1] && array[i + 1] < array[i + 2]))) {
                isZigZagUpwards = false;
                break;
            }
        }
        if (isZigZagUpwards) {
            System.out.println("The sequence is zigzagged upwards.");
        } else {
            System.out.println("The sequence is not zigzagged upwards.");
        }
    }
}
