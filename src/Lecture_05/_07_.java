package Lecture_05;

import java.util.Scanner;

public class _07_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }
        int[] array2 = new int[arrSize];
        array2[0] = array[0];
        array2[arrSize - 1] = array[arrSize - 1];

        for (int i = 1; i < array2.length - 1; i++) {
            array2[i] = array[i - 1] + array[i + 1];
        }

        System.out.print("Print new array : ");

        for (int i = 0; i < array2.length; i++) {
            System.out.print(array2[i] + " ");
        }
    }
}
