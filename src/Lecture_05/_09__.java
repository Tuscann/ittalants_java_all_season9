package Lecture_05;

import java.util.Scanner;

public class _09__ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }

        for (int i = 0; i < array.length / 2; i++) {

            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }

        for(int i = 0; i < array.length; i++) {
            if(i == array.length - 1) {
                System.out.println(array[i]);
            }
            else {
                System.out.print(array[i] + ", ");
            }
        }
    }
}
