package Lecture_05;

import java.util.Scanner;

public class _18_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the length of the two arrays:");
        int a = Integer.parseInt(scanner.nextLine());

        int[] array = new int[a];
        int[] array2 = new int[a];

        System.out.println("Fill the array 1:");

        for (int i = 0; i < array.length; i++) {
            System.out.print("Enter element number " + (i + 1) + " for the array : ");
            array[i] = Integer.parseInt(scanner.nextLine());
        }
        System.out.println("Fill the array 2:");
        for (int i = 0; i < array2.length; i++) {
            System.out.print("Enter element number " + (i + 1) + " for the array : ");
            array2[i] = Integer.parseInt(scanner.nextLine());
        }

        int[] array3 = new int[a];

        for (int i = 0; i < array2.length; i++) {
            if (array[i] >= array2[i]) {
                array3[i] = array[i];
            } else {
                array3[i] = array2[i];
            }
        }
        System.out.print("Array 1 = ");
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                System.out.print(array[i] + ", ");
            } else {
                System.out.print(array[i]);
            }
        }
        System.out.println();
        System.out.print("Array 2 = ");
        for (int i = 0; i < array2.length; i++) {
            if (i < array2.length - 1) {
                System.out.print(array2[i] + ", ");
            } else {
                System.out.print(array2[i]);
            }
        }
        System.out.println();
        System.out.print("Array 3 = ");
        for (int i = 0; i < array3.length; i++) {
            if (i < array3.length - 1) {
                System.out.print(array3[i] + ", ");
            } else {
                System.out.print(array3[i]);
            }
        }
    }
}
