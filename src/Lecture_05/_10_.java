package Lecture_05;

import java.util.Scanner;

public class _10_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];
        double sum = 0;

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
            sum += array[i];
        }
        double avgValue = sum / array.length;

        double deferenceSmallest = 999999999;
        int nimber = 0;

        for (int i = 0; i < array.length; i++) {

            double sub = Math.abs(array[i] - avgValue);

            if (deferenceSmallest > sub) {
                deferenceSmallest = sub;
                nimber = array[i];
            }
        }
        System.out.println("Average value is " + avgValue + ", nearest value " + nimber);
    }
}
