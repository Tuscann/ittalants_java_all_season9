package Lecture_05;

import java.util.Scanner;

public class _15__ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the length of the array:");
        int a = Integer.parseInt(scanner.nextLine());

        double[] array = new double[a];

        System.out.println("Fill the array:");

        for(int i = 0; i < array.length; i++){
            array[i] = Double.parseDouble(scanner.nextLine());
        }

        boolean isSorted = true;
        while(isSorted){

            isSorted = false;

            for (int i = 0; i < array.length - 1; i++) {
                if(array[i] < array[i + 1]){
                    double temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    isSorted = true;
                }
            }
        }

        System.out.println(array[0] + " " + array[1] + " " + array[2]);
    }
}
