package Lecture_05;

import java.util.Scanner;

public class _15_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the length of arr:");
        int arr1Size = Integer.parseInt(sc.nextLine());
        double[] array = new double[arr1Size];

        System.out.println("Enter elements of arr1:");

        for (int i = 0; i < array.length; i++) {
            double current = Double.parseDouble(sc.nextLine());

            if (current < 0) {
                current *= -1;
            }
            array[i] = current;
        }

        double maxOne = Double.MIN_VALUE;
        double maxTwo = Double.MIN_VALUE;
        double maxThree = Double.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            /* If current element is smaller than first*/
            if (array[i] > maxOne) {
                maxThree = maxTwo;
                maxTwo = maxOne;
                maxOne = array[i];
            }
            /* If arr[i] is in between first and second then update second  */
            else if (array[i] > maxTwo) {
                maxThree = maxTwo;
                maxTwo = array[i];
            } else if (array[i] > maxThree) {
                maxThree = array[i];
            }
        }
        System.out.println(maxOne + "; " + maxTwo + "; " + maxThree);
    }
}
