package Lecture_05;

import java.util.Arrays;
import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a lengh of array : ");
        int length = scan.nextInt();

        if(length % 2 == 0) {
            int[] matrix = new int[length];

            for (int i = 0; i < length; i++) {
                System.out.printf("Enter value for cell %d: ", i + 1);
                matrix[i] = scan.nextInt();
            }
            int[] newMatrix = new int[length];

            for (int i = 0; i < matrix.length; i++) {
                if (i < matrix.length / 2) {
                    newMatrix[i] = matrix[i];
                } else {
                    newMatrix[i] = matrix[matrix.length - i + 1];
                }
                System.out.print(newMatrix[i]+" ");
            }
        }
    }
}
