package Lecture_05;

import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }
        int smallestNumber = 999999999;

        for (int i = 0; i < arrSize; i++) {
            if (array[i] % 3 == 0 && array[i] < smallestNumber) {
                smallestNumber = array[i];
            }
        }

        if (smallestNumber == 999999999) {
            System.out.println("Array not have numbers divide by 3 :");
        } else {
            System.out.println("The smallest number divide by 3 is = " + smallestNumber);
        }
    }
}
