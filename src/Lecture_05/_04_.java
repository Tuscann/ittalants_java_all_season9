package Lecture_05;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a lenght of array");
        int n = scan.nextInt();

        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = scan.nextInt();
        }
        System.out.print("Printed array : ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        boolean isFound = true;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != array[array.length - i - 1]) {
                isFound = false;
            }
            if (!isFound) {

                System.out.println("The array is not symmetric");
                return;
            }
        }
        System.out.println("The array is symmetric.");
    }
}
