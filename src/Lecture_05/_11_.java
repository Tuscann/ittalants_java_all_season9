package Lecture_05;

import java.util.Scanner;

public class _11_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];
        int count = 0;

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 5 == 0 && array[i] > 5) {
                System.out.print(array[i]+" ");
                count++;
            }
        }
        System.out.println("- " + count + " numbers");
    }
}
