package Lecture_05;

import java.util.Scanner;

public class _12_ {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int[] array = new int[7];

        for (int i = 0; i < array.length; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = scan.nextInt();
        }

        for (int i = 0; i < array.length; i++) {

            if (i == 0) {                //exchange first and second value of the array by third variable

                int current = array[0];
                array[0] = array[1];
                array[1] = current;
            } else if (i == 2) {         //exchange third and fourth value of the array by sum

                array[2] = array[2] + array[3];
                array[3] = array[2] - array[3];
                array[2] = array[2] - array[3];
            } else if (i == 4) {           //exchange fifth and sixth value of the array by multiplication

                array[4] = array[4] * array[5];
                array[5] = array[4] / array[5];
                array[4] = array[4] / array[5];
            }
            System.out.print(array[i] + " ");
        }
    }
}
