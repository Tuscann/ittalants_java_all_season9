package Lecture_05;

import java.util.Scanner;

public class _09_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }

        int[] array2 = new int[arrSize];

        for(int i = 0; i < array2.length; i++) {
            array2[i] = array[array.length - i - 1];
            if(i == array2.length - 1) {
                System.out.println(array2[i]);
            }
            else {
                System.out.print(array2[i] + ", ");
            }
        }
    }
}
