package Lecture_05;

import java.util.Scanner;

public class _13_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();
        int start = n;

        int newNumber = n;

        int sizeOfBinArray = 0;
        while (newNumber > 0) {
            newNumber = newNumber / 2;
            sizeOfBinArray++;
        }

        int[] array = new int[sizeOfBinArray];

        for (int i = 0; i < array.length; i++) {
            array[i] = n % 2;
            n = n / 2;
        }
        System.out.print("Decimal number to binary number : " + start + " = ");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i]);
        }


    }
}
