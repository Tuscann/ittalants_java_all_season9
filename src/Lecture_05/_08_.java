package Lecture_05;

import java.util.Scanner;

public class _08_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the array :");
        int arrSize = sc.nextInt();

        int[] array = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            System.out.printf("Enter value for cell %d: ", i + 1);
            array[i] = sc.nextInt();
        }

        int currCounter = 1;
        int index = 0;
        int bestCounter = 1;

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                currCounter++;

            } else {
                currCounter = 1;
            }
            if (currCounter > bestCounter) {
                bestCounter = currCounter;
                index = i;
            }
        }
        for (int i = 0; i < bestCounter; i++) {
            if (i == bestCounter - 1) {
                System.out.println(array[index]);
            } else {
                System.out.print(array[index] + ", ");
            }
        }
    }
}
