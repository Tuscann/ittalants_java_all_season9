package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _20_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n - 1 - row; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col <= row; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (col > row) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
