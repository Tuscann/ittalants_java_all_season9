package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _27_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < 2 * numberN - 1; row++) {
            if (row == 0 || row == numberN - 1 || row == 2 * numberN - 2) {
                System.out.print(" ");
                for (int col = 0; col < numberN - 2; col++) {
                    System.out.print("*");
                }
                System.out.println(" ");
            } else {
                System.out.print("*");
                for (int col = 0; col < numberN - 2; col++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
        }
    }
}
