package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _19_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < n; col++) {
                if (col <= row || col == 2 * (n - row) - 1) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (col < n - row) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
