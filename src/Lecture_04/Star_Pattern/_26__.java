package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _26__ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN - 1; row++) {
            for (int col = 0; col < row; col++) {
                System.out.print(" ");
            }
            System.out.print("*");
            for (int col = 0; col < 2 * numberN - 3 - 2 * row; col++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }

        for (int col = 0; col < numberN - 1; col++) {
            System.out.print(" ");
        }
        System.out.println("*");

        for (int row = 0; row < numberN - 1; row++) {
            for (int col = 0; col < numberN - 2 - row; col++) {
                System.out.print(" ");
            }
            System.out.print("*");
            for (int col = 0; col < 2 * row + 1; col++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }
    }
}
