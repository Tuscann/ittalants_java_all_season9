package Lecture_04.Star_Pattern;

public class _26_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < 2 * n - 1; row++) {
            for (int col = 0; col < 2 * n - 1; col++) {
                if (row == col || row == 2 * n - col - 2) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
