package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _06_ {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < row; col++) {
                System.out.print(" ");
            }
            System.out.print("*");
            if (row == 0 || row == n - 1) {
                for (int col = 0; col < n - 2; col++) {
                    System.out.print("*");
                }
            } else {
                for (int col = 0; col < n - 2; col++) {
                    System.out.print(" ");
                }
            }
            System.out.println("*");
        }
    }
}
