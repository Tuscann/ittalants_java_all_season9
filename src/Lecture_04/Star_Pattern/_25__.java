package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _25__ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();

        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < n - 1; col++) {
                System.out.print(" ");
            }
            System.out.println("+");
        }

        for (int col = 0; col < 2 * n - 1; col++) {
            System.out.print("+");
        }
        System.out.println();

        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < n - 1; col++) {
                System.out.print(" ");
            }
            System.out.println("+");
        }
    }
}
