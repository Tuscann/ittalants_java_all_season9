package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _28_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN - 2; row++) {
            for (int col = 0; col < numberN - 3 - row; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < numberN + 2 * row; col++) {
                System.out.print("*");
            }
            for (int col = 0; col < 2 * (numberN - row) - 5; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < numberN + 2 * row; col++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int row = 0; row < 3 * numberN - 5; row++) {
            for (int col = 0; col < row; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 6 * numberN - 2 * row - 11; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
