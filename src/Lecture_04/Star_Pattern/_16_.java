package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _16_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 1; row <= n; row++) {
            for (int col = 0; col < n; col++) {
                if (row == n) {
                    System.out.print("**");
                } else {
                    if (col == n - row  ) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }


            }
            System.out.println();
        }
    }
}
