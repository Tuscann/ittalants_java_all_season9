package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _12__ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            if (row == 0) {
                for (int col = 0; col < numberN; col++) {
                    System.out.print("*");
                }
                System.out.println();
            } else if (row == numberN - 1) {
                System.out.println("*");
            } else {
                System.out.print("*");
                for (int col = 0; col < numberN - 2 - row; col++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
        }
    }
}
