package Lecture_04.Star_Pattern;

public class _08_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col <= row; col++) {
                if (row == n - 1 || col == 0||col==row) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
