package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _24_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < numberN - row; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < numberN - row; col++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int row = 0; row < numberN - 1; row++) {
            for (int col = 0; col < row + 2; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < row + 2; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
