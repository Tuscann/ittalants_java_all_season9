package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _15_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < numberN - row - 1; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * row + 1; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
