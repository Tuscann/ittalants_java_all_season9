package Lecture_04.Star_Pattern;

import java.util.Scanner;

public class _16__ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;

        for (int row = 0; row < numberN; row++) {
            if (row == 0) {
                for (int col = 0; col < numberN - 1; col++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            } else if (row == numberN - 1) {
                for (int col = 0; col < 2 * numberN - 1; col++) {
                    System.out.print("*");
                }
                System.out.println();
            } else {
                for (int col = 0; col < numberN - 1 - row; col++) {
                    System.out.print(" ");
                }
                System.out.print("*");
                for (int col = 0; col < 2 * row - 1; col++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
        }
    }
}
