package Lecture_04.Star_Pattern;

public class _12_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n - row; col++) {
                if (row == 0 || col == 0 || col == n - 1 - row) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
