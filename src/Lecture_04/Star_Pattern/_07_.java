package Lecture_04.Star_Pattern;

public class _07_ {
    public static void main(String[] args) {
        int n = 5;

//        for (int row = 0; row < n; row++) {
//            for (int col = 0; col <= row; col++) {
//                System.out.print("*");
//            }
//            System.out.println();
//        }
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (col <= row || col == 2 * (n - row) - 1) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
