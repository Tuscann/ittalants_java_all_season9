package Lecture_04.Star_Pattern;

public class _25_ {
    public static void main(String[] args) {

        int n = 5;

        for (int row = 0; row < 2 * n - 1; row++) {
            for (int col = 0; col < 2 * n - 1; col++) {
                if (col == n - 1 || row == n - 1) {
                    System.out.print("+");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
