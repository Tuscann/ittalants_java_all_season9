package Lecture_04.Number_Pattern;

public class _05_ {
    public static void main(String[] args) {
        int n = 5;
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row == n / 2 && col == n / 2){
                    System.out.print("0");
                }
                else {
                    System.out.print("1");
                }
            }
            System.out.println();
        }
    }
}
