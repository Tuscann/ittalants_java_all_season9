package Lecture_04.Number_Pattern;

public class _06__ {
    public static void main(String[] args) {

        int n = 5;
        int x = 1;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.print(x);
                if (x == 1) {
                    x = 0;
                } else {
                    x = 1;
                }
            }
            System.out.println();
        }
    }
}
