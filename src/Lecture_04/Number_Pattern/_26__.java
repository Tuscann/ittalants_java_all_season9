package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _26__ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < row + 1; col++) {
                System.out.print(row + 1 - col+" ");
            }
            System.out.println();
        }
    }
}
