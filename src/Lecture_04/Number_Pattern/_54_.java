package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _54_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 12345;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n - row; col++) {
                System.out.print(col + row + 1);
            }
            System.out.println();
        }
    }
}
