package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _15_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 1; row <= n; row++) {
            int counter = row;
            for (int col = 0; col < n; col++) {
                System.out.print(counter);
                counter++;
                if (counter > n) {
                    counter = n;
                }
            }
            System.out.println();
        }
    }
}
