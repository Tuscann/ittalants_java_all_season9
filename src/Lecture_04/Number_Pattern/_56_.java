package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _56_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * row + 1; col++) {
                System.out.print(col + 1);
            }
            System.out.println();
        }

        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < 2 * (n - row) - 3; col++) {
                System.out.print(col + 1);
            }
            System.out.println();
        }
    }
}
