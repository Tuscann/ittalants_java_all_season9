package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _43_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            int counter = 0;
            for (int col = 0; col < 2 * row + 1; col++) {
                if (col <= row) {
                    counter += 1;
                } else {
                    counter -= 1;
                }
                System.out.print(counter + " ");
            }
            System.out.println();
        }
    }
}
