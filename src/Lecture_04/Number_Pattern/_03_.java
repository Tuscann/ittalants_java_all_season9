package Lecture_04.Number_Pattern;

public class _03_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int cols = 1; cols <= n; cols++) {
                if (cols % 2 == 0) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
    }
}
