package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _44_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();
        int counter = 0;
        int everyRow = 1;

        for (int row = 0; row < n; row++) {
            if (row != 0) {
                everyRow = 2 * row;
            }
            for (int col = 0; col < everyRow; col++) {
                if (row == 0) {
                    System.out.print(1);
                }
                else if (col < row) {
                    counter = row + 2 + col;
                    System.out.print(counter + " ");
                }
                else {
                    counter -= 1;
                    System.out.print(counter + " ");
                }
            }
            System.out.println();
        }
    }
}
