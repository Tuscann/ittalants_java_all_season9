package Lecture_04.Number_Pattern;

public class _09_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (col == row || col == n - row - 1) {
                    System.out.print("0");
                } else if (row >= 1 && col >= 1 && row < n - 1 && col < n - 1) {
                    System.out.print("0");
                } else {
                    System.out.print("1");
                }
            }
            System.out.println();
        }
    }
}
