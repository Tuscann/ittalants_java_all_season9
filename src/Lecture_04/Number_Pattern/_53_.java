package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _53_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 12345*10;

        for (int row = 1; row <= n; row++) {
            n /= 10;
            System.out.println(n);
        }
    }
}
