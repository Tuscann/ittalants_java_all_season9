package Lecture_04.Number_Pattern;

public class _22_ {
    public static void main(String[] args) {
        int n = 5;

        int counter = 1;

        for (int i = 0; i < n; i++) {
            for (int col = 0; col < n; col++) {
                if (i <= col) {
                    System.out.print(counter);
                }
            }
            counter += 1;
            System.out.println();
        }
    }
}
