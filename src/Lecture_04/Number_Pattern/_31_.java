package Lecture_04.Number_Pattern;

public class _31_ {
    public static void main(String[] args) {
        int n = 5;
        System.out.println("Write a number");

        for (int row = 0; row < n; row++) {
            int counter = 1+row;
            for (int col = 0; col < n; col++) {
                if (row <= col) {
                    System.out.print(counter + " ");
                    counter += 1;
                }
            }
            System.out.println();
        }
    }
}
