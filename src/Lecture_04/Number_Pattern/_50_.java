package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _50_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;
        int counter = 1;
        int sum = 1;

        for (int row = 1; row <= numberN; row++) {
            for (int col = 0; col < row; col++) {
                System.out.print(sum + " ");
                sum += counter;
                counter++;
            }
            System.out.println();
        }
    }
}
