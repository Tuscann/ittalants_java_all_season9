package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _59_ {
    public static void main(String[] args) {
        System.out.println("Write a number : ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        // first n-1 rows
        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < row; col++) {
                System.out.print(" ");
            }
            System.out.print(row + 1);
            for (int col = 0; col < 2 * (n - row) - 3; col++) {
                System.out.print(" ");
            }
            System.out.println(row + 1);
        }

        // middle row
        for (int row = 0; row < n - 1; row++) {
            System.out.print(" ");
        }
        System.out.print(n);
        for (int row = 0; row < n - 1; row++) {
            System.out.print(" ");
        }
        System.out.println();

        // last n-1 rows
        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < n - 2 - row; col++) {
                System.out.print(" ");
            }
            System.out.print(n - row - 1);
            for (int col = 0; col < 2 * row + 1; col++) {
                System.out.print(" ");
            }
            System.out.println(n - row - 1);
        }
    }
}
