package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _28_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();

        for (int row = 0; row < n; row++) {
            int counter = n;
            for (int col = 0; col < row + 1; col++) {
                System.out.print(counter+ " ");
                counter-=1;
            }
            System.out.println();
        }
    }
}
