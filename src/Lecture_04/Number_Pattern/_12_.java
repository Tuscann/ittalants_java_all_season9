package Lecture_04.Number_Pattern;

public class _12_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 1; col <= n; col++) {
                System.out.print(row+col);
            }
            System.out.println();
        }
    }
}
