package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _52_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col <= row; col++) {
                if (row < numberN / 2) {
                    System.out.print(row + 1);
                } else {
                    System.out.print(numberN - row);
                }
            }
            System.out.println();
        }
    }
}
