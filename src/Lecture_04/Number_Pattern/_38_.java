package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _38_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();

        int counter = 1;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < row + 1; col++) {

                if (col == 0 || col == row || row == n - 1) {
                    counter = 1;
                } else {
                    counter = 0;
                }
                System.out.print(counter + " ");
            }
            System.out.println();
        }
    }
}
