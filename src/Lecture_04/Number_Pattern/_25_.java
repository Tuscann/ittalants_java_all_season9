package Lecture_04.Number_Pattern;

public class _25_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            int counter = 0;
            for (int col = 0; col < n; col++) {
                if (row <= col) {
                    System.out.print(++counter);
                }
            }
            System.out.println();
        }
    }
}
