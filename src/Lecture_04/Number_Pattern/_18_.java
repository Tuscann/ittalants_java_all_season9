package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _18_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < 2 * n - 1; row++) {
            int counter = n;
            for (int col = 0; col < 2 * n - 1; col++) {
                if (row == col) {
                    counter = n - row * col;
                }
//                if (row == n - col-row) {
//                    counter += 1;
//                }
                System.out.print(counter);
            }
            System.out.println();
        }
    }
}
