package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _58_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();
        int counter = 0;

        System.out.println("*");
        for (int row = 0; row < numberN; row++) {
            System.out.print("*");
            for (int col = 0; col < 2 * row + 1; col++) {
                if (row >= col) {
                    counter++;
                } else {
                    counter--;
                }
                System.out.print(counter);
            }
            counter = 0;
            System.out.println("*");
        }

        for (int row = 0; row < numberN - 1; row++) {
            System.out.print("*");
            for (int col = 0; col < 2 * numberN - 3 - 2 * row; col++) {
                if (col < numberN - 1 - row) {
                    counter++;
                } else {
                    counter--;
                }
                System.out.print(counter);
            }
            counter = 0;
            System.out.println("*");
        }
        System.out.println("*");
    }
}
