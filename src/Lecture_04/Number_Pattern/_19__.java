package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _19__ {
    public static void main(String[] args) {
        System.out.println("Enter The Value For N :");

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] spiral = new int[n][n];

        int currentValue = 1;
        int minCol = 0;
        int maxCol = n - 1;
        int minRow = 0;
        int maxRow = n - 1;

        while (currentValue <= n * n) {
            for (int i = minCol; i <= maxCol; i++) {
                spiral[minRow][i] = currentValue++;
            }

            for (int i = minRow + 1; i <= maxRow; i++) {
                spiral[i][maxCol] = currentValue++;
            }

            for (int i = maxCol - 1; i >= minCol; i--) {
                spiral[maxRow][i] = currentValue++;
            }

            for (int i = maxRow - 1; i >= minRow + 1; i--) {
                spiral[i][minCol] = currentValue++;
            }

            minCol++;
            minRow++;
            maxCol--;
            maxRow--;
        }

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.print(spiral[row][col] + " ");
            }
            System.out.println();
        }
    }

}
