package Lecture_04.Number_Pattern;

public class _19_ {
    public static void main(String[] args) {

        int dimension = 5;

        int[][] spiralArray = new int[dimension][dimension];

        int numConcentricSquares = dimension / 2 + 1;

        int j;
        int sideLen = dimension;
        int currNum = 1;

        for (int row = 0; row < numConcentricSquares; row++) {

            for (j = 0; j < sideLen; j++) {
                spiralArray[row][row + j] = currNum++;      // do top side
            }
            for (j = 1; j < sideLen; j++) {
                spiralArray[row + j][dimension - 1 - row] = currNum++;    // do right side
            }
            for (j = sideLen - 2; j > -1; j--) {
                spiralArray[dimension - 1 - row][row + j] = currNum++;    // do bottom side
            }
            for (j = sideLen - 2; j > 0; j--) {
                spiralArray[row + j][row] = currNum++;    // do left side
            }
            sideLen -= 2;
        }

        for (int row = 0; row < dimension; row++) {
            for (int col = 0; col < dimension; col++) {
                System.out.printf("%d ", spiralArray[row][col]);
            }
            System.out.println();
        }
    }
}
