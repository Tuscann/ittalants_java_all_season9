package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _47_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;
        int counter = 1;
        int counterCols = 1;

        for (int row = 1; row <= n; row++) {
            if (row != 1) {
                counterCols *= 2;
            }
            for (int col = 0; col < counterCols; col++) {
                System.out.print(counter++);
                if (counter == 10) {
                    counter = 1;
                }
            }
            System.out.println();
        }
    }
}
