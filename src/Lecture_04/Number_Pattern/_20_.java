package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _20_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a integer : ");

        int n = 5;
        int counter = 1;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row >= col) {
                    System.out.print(counter);
                }
            }
            counter += 1;
            System.out.println();
        }
    }
}
