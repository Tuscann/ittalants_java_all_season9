package Lecture_04.Number_Pattern;

public class _27_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            int counter = n - row;
            for (int col = 0; col < n; col++) {
                if (row <= col) {
                    System.out.print(counter + " ");
                    counter -= 1;
                }
            }
            System.out.println();
        }
    }
}
