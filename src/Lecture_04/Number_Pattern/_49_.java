package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _49_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            int step = 4;
            int sum = row + 1;
            for (int col = 0; col < row + 1; col++) {
                System.out.print(sum+" ");
                sum += step;
                step--;
            }
            System.out.println();
        }
    }
}
