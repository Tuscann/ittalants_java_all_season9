package Lecture_04.Number_Pattern;

public class _10_ {
    public static void main(String[] args) {
        int n = 5;
        for (int row = 1; row <= n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.print(row);
            }
            System.out.println();
        }
    }
}
