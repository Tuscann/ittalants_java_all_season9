package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _14_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = 5;
        int counter = numberN;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < numberN; col++) {
                if (col < row) {
                    System.out.print(counter + " ");
                    counter--;
                }
                else {
                    System.out.print(counter + " ");
                }
            }
            counter = numberN;
            System.out.println();
        }
    }
}
