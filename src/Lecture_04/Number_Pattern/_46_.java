package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _46_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = sc.nextInt();

        int counter = 1;
        for (int row = 0; row < n; row++) {
            if (row % 2 == 0) {
                counter = 1;
            }
            else {
                counter = row + 1;
            }
            for (int col = 0; col < row + 1; col++) {
                if (row % 2 == 0) {
                    System.out.print(counter + col + " ");
                }
                else {
                    System.out.print(counter - col + " ");
                }
            }
            System.out.println();
        }
    }
}
