package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _41_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            int counter = -1;
            for (int col = 0; col < 2 * row + 1; col++) {
                if (col <= row) {
                    counter += 2;
                } else {
                    counter -= 2;
                }
                System.out.print(counter + " ");
            }
            System.out.println();
        }
    }
}
