package Lecture_04.Number_Pattern;

public class _17_ {
    public static void main(String[] args) {
        int n = 5;

        for (int row = 0; row < n; row++) {
            int counter = row + 2;
            for (int col = 0; col < n; col++) {
                if (row >= col) {
                    counter--;
                } else {
                    counter++;
                }
                System.out.print(counter);
            }
            System.out.println();
        }
    }
}
