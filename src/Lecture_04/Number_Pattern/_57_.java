package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _57_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;
        int counter = 0;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * row + 1; col++) {
                if (row >= col) {
                    counter++;
                }
                else {
                    counter--;
                }
                System.out.print(counter);
            }
            counter = 0;
            System.out.println();
        }

        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < 2 * n - 3 - 2 * row; col++) {
                if (col < n - 1 - row) {
                    counter++;
                }
                else {
                    counter--;
                }
                System.out.print(counter);
            }
            counter = 0;
            System.out.println();
        }
    }
}
