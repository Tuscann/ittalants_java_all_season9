package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _40_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < row + 1; col++) {
                int x = 2 * col + 1;

                if (row % 2 != 0) {
                    x += 1;
                }
                System.out.print(x + " ");
            }
            System.out.println();
        }
    }
}
