package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _23_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = 5;
        int counter = n;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row >= col) {
                    System.out.print(counter);
                }
            }
            counter -= 1;
            System.out.println();
        }
    }
}
