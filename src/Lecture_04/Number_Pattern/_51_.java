package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _51_ {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;
        int counter = 1;

        for (int row = 0; row < n; row++) {
            if (row % 2 == 0) {
                for (int col = 0; col <= row; col++) {
                    System.out.print(counter+" ");
                    counter++;
                }
            }
            else {
                counter = counter + row + 1;
                for (int col = 0; col <= row; col++) {
                    counter--;
                    System.out.print(counter+" ");
                }
                counter = counter + row + 1;
            }
            System.out.println();
        }
    }
}
