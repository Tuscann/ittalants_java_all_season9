package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _26_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            int x = row + 1;
            for (int col = 0; col < row + 1; col++) {

                System.out.print(x + " ");
                x -= 1;
            }
            System.out.println();
        }
    }
}
