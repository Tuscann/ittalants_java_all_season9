package Lecture_04.Number_Pattern;

import java.util.Scanner;

public class _48_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int n = 5;

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < row + 1; col++) {
                System.out.print(col + 1 + " ");
            }
            for (int col = 0; col < 2 * (n - row) - 2; col++) {
                System.out.print("  ");
            }
            for (int col = row + 1; col > 0; col--) {
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }
}
