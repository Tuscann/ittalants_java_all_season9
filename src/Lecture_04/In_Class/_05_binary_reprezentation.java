package Lecture_04.In_Class;

import java.util.Scanner;

public class _05_binary_reprezentation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number : ");

        int number = sc.nextInt();
        int rest;
        int sum = 0;
        int mnojitel = 1;

        while (number > 0) {
            rest = number % 2;
            sum += rest * mnojitel;
            mnojitel *= 10;
            number /= 2;
        }
        System.out.println("Binary representation : " + sum);
    }
}
