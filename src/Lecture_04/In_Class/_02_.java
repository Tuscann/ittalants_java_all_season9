package Lecture_04.In_Class;

public class _02_ {
    public static void main(String[] args) {

        for (int i = 0; i <= 100; i++) {
            if (i > 0 && i <= 50) {
                if (i % 2 != 0) {
                    System.out.println(i);
                }
            } else {
                if (i % 2 == 0) {
                    System.out.println(i);
                }
            }
        }
    }
}
