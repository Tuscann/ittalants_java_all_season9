package Lecture_04.In_Class;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();

        int max = b;
        int min = a;

        if (a > b) {
            max = a;
            min = b;
        }

        for (int i = min; i < max; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }


    }
}
