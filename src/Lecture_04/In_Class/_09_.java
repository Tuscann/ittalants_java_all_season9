package Lecture_04.In_Class;

import java.util.Scanner;

public class _09_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number: ");
        int n = Integer.parseInt(scan.nextLine());

        int before = n / 2;
        int after = n / 2;

        for (int row = 0; row < n / 2; row++) {
            for (int i = 0; i < before; i++) {  // free spaces before triangle
                System.out.print(" ");
            }
            for (int i = 0; i < n - before - after; i++) {
                if (i == 0 || i == n - before - after - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            before--;
            after--;
            System.out.println();
            if (row == n / 2 - 1) {  // last row print
                for (int rows = 0; rows < n; rows++) {
                    System.out.print("*");
                }
            }
        }
    }
}
