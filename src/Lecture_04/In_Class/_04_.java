package Lecture_04.In_Class;

import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {


        System.out.println("Enter any number:");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        scan.close();

        int temp;
        boolean isPrime = true;

        for (int i = 2; i <= num / 2; i++) {
            temp = num % i;
            if (temp == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            System.out.println(num + " is a Prime Number");
        } else {
            System.out.println(num + " is not a Prime Number");
        }
    }
}
