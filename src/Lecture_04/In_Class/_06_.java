package Lecture_04.In_Class;

import java.util.Scanner;

public class _06_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number : ");

        int number = sc.nextInt();
        int original = number;
        int positiveNum = number;
        int sum = 0;
        int mnojitel = 1;

        while (number > 0) {
            int rest = number % 2;
            sum += rest * mnojitel;
            mnojitel *= 10;
            number /= 2;
        }

        System.out.printf("Binary representation %d = %d\n", original, sum);

//        int counterOne = 0;
//        while (original != 0) {
//            original = original & (original - 1);
//            counterOne++;
//        }
//        System.out.println("Count of 1 = "+counterOne);

        int counterOnes = 0;

        while (positiveNum > 0) {
            if (positiveNum % 2 == 1) {
                counterOnes++;
            }
            positiveNum /= 2;
        }
        System.out.println("The bits which are ones are " + counterOnes);
    }
}
