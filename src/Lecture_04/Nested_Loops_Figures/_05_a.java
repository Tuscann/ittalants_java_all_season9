package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _05_a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write integer number : ");  // 12
        int n = Integer.parseInt(scan.nextLine());

        if (n % 2 == 0) {
            for (int row = 0; row <= n / 2; row++) {
                for (int col = 0; col < n; col++) {
                    if (row + col >= n - 1) {
                        System.out.print(" ");
                    } else if (row <= col || row == n - 1) {
                        System.out.print("# ");
                    } else {
                        System.out.print("  ");
                    }
                }
                System.out.println();
            }
        } else {
            for (int row = 0; row <= n / 2; row++) {
                for (int col = 0; col < n; col++) {

                    if (row <= col / 2) {
                        System.out.print("#");
                    }
                    System.out.print(" ");
                }
                System.out.println();
            }
        }
    }
}
