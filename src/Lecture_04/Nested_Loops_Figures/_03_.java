package Lecture_04.Nested_Loops_Figures;

public class _03_ {
    public static void main(String[] args) {

        System.out.print("* | ");
        for (int i = 0; i <= 8; i++) {
            System.out.print(i + 1 + " ");
        }
        System.out.println();
        for (int i = 0; i < 22; i++) {
            System.out.print("-");
        }
        System.out.println();

        for (int rows = 1; rows < 10; rows++) {
            System.out.print(rows + " | ");
            for (int cols = 1; cols < 10; cols++) {
                if (cols == 9) {
                    System.out.print(rows * cols);
                }
                else {
                    System.out.print(rows * cols + (" "));
                }
            }
            System.out.println();
        }
    }
}