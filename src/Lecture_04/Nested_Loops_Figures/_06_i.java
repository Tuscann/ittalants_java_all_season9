package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_i {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
           int counter = 0;

            for (int col = 0; col < 2 * row; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * numberN - 1 - 2 * row; col++) {
                if (col < numberN - row) {
                    counter++;
                }
                else {
                    counter--;
                }

                if (col >= 2 * numberN - 2 - 2 * row) {
                    System.out.println(counter);
                }
                else {
                    System.out.print(counter + " ");
                }
            }
        }
    }
}
