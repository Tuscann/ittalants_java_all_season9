package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _07_a {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            int counter = 1;
            for (int col = 0; col < 2 * (numberN- row) - 1; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * row + 1; col++) {
                if (col < row + 1) {
                    System.out.print(counter + " ");
                    counter *= 2;
                    if (col == row) {
                        counter /= 2;
                    }
                }
                else {
                    counter /= 2;
                    System.out.print(counter + " ");
                }
            }
            System.out.println();
        }
    }
}
