package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _04_d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int rows = 0; rows < n; rows++) {
            for (int col = n; col > 0; col--) {
                if (rows < col) {
                    System.out.print("  ");
                }
                else {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
    }
}
