package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_l {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();
        int counter = 0;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < 2 * (numberN-row) - 1; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * row + 1; col++) {
                if (col < row + 1) {
                    counter++;
                    if (counter == 10) {
                        counter = 0;
                    }
                    System.out.print(counter + " ");
                }
                else {
                    counter--;
                    if (counter == -1) {
                        counter = 9;
                    }
                    System.out.print(counter + " ");
                }
            }
            System.out.println();
        }
    }
}
