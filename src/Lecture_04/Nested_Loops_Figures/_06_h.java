package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_h {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * (n - row) - 1; col++) {  // double free spaces
                System.out.print(" ");
            }
            int counter = 0;
            for (int col = row; col >= 0; col--) {
                counter++;
                System.out.print(counter);

                if (col != 0) {
                    System.out.print(" ");
                }
            }
            System.out.print(" ");
            for (int col = row; col > 0; col--) {
                counter--;
                System.out.print(counter + " ");
            }
            System.out.println();
        }
    }
}
