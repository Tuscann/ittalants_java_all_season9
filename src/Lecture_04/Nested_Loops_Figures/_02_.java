package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a number : ");
        int n = scan.nextInt();

        for (int row = 1; row <= n; row++) {
            for (int colums = 1; colums <= n; colums++) {
                if (row % 2 == 0) {
                    System.out.print(" #");
                } else if (row % 2 != 0) {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
    }
}
