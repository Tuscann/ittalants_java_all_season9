package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _11_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int step = 0; step < n; step++) {

            print11(n, step);
            System.out.print("  o  ******");  //row 1 with head
            printttt(step);

            print11(n, step);
            System.out.print(" /|\\ *");     //row 2 with hands and body
            printttt(step + 1);

            print11(n, step);
            System.out.print(" / \\ *");     //row 3 with legs
            printttt(step + 1);
        }
        for (int col = 0; col < 5 * (n + 1) + 2; col++) {
            System.out.print("*");
        }
        System.out.println();
    }

    private static void print11(int n, int step) {
        for (int col = 0; col < (n - 1) * 5 - 5 * step; col++) {
            System.out.print(" ");
        }
    }

    private static void printttt(int i) {
        for (int col = 0; col < i * 5; col++) {
            System.out.print(" ");
        }
        System.out.println("*");
    }
}
