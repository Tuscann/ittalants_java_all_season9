package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a integer : ");

        int n = scan.nextInt();

        for (int rows = 0; rows <= n; rows++) {
            int counter = 1;
            for (int col = 0; col <= n; col++) {
                if (rows > col) {
                    System.out.print(counter + " ");
                    counter++;
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
