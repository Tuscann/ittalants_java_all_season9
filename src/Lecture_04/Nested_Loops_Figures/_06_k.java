package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_k {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();
        int counter = 0;

        for (int col = 0; col < 2 * numberN - 1; col++) {
            if (col < numberN) {
                counter++;
            }
            else {
                counter--;
            }
            if (col < 2 * numberN - 2) {
                System.out.print(counter + " ");
            }
            else {
                System.out.println(counter);
            }
        }

        counter = 0;
        for (int row = 0; row < numberN - 1; row++) {
            for (int col = 0; col < numberN - 1 - row; col++) {
                counter++;
                System.out.print(counter + " ");
            }
            for (int col = 0; col < 4 * row + 1; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < numberN - 1 - row; col++) {
                System.out.print(" " + counter);
                counter--;
            }
            System.out.println();
        }
    }
}
