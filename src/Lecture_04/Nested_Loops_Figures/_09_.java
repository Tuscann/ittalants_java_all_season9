package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _09_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n - row - 1; col++) {
                System.out.print(".");
            }

            for (int col = 0; col < row + 1; col++) {
                System.out.print(row + 1);
            }
            System.out.println();
        }
    }
}
