package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _02__ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Add numbers : ");
        int number = Integer.parseInt(scan.nextLine());

        for (int row = 0; row < number; row++) {
            for (int col = 0; col < number; col++) {
                if (row % 2 != 0 && col == 0) {
                    System.out.print(" ");
                }
                if (col == number - 1) {
                    System.out.print("#");
                } else {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
    }
}
