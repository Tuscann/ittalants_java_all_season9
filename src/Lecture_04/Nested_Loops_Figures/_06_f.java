package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_f {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a integer : ");

        int n = scan.nextInt();

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * (n -  row) - 1; col++) {  // double free spaces
                System.out.print(" ");
            }

            for (int col = row; col >= 0; col--) {
                if (col != 0) {
                    System.out.print((col + 1) + " ");
                }
                else {
                    System.out.println(col + 1);
                }
            }
        }
    }
}
