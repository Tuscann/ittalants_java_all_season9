package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _07_c {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();
        int counter = 1;

        for (int row = 0; row < numberN; row++) {
            for (int col = 0; col < 2 * numberN - 2 - 2 * row; col++) {
                System.out.print(" ");
            }
            counter = 1;
            for (int col = 0; col <= row; col++) {
                System.out.print(counter + "   ");
                counter = counter * (row - col) / (col + 1);
            }
            System.out.println();
        }
    }
}
