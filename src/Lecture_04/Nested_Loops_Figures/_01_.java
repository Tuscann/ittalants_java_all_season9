package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a number : ");
        int n = scan.nextInt();

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.print("# ");
            }
            System.out.println();
        }

    }
}
