package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _06_e_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Write a number : ");

        int n = Integer.parseInt(scan.nextLine());
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * row; col++) {
                System.out.print(" ");
            }

            for (int col = 0; col < n - row; col++) {
                if (col != n - row - 1) {
                    System.out.print((col + 1) + " ");
                }
                else {
                    System.out.println((col + 1));
                }
            }
        }
    }
}
