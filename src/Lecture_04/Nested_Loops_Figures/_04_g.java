package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _04_g {
    public static void main(String[] args) {

        System.out.println("Add number : ");
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();

        for (int rows = 0; rows < n; rows++) {
            for (int colums = 0; colums < n; colums++) {
                if (rows == 0 || rows == n - 1) {
                    System.out.print("# ");
                } else {
                    if (rows + colums + 1 == n) {
                        System.out.print("# ");
                    } else {
                        System.out.print("  ");
                    }
                }
            }
            System.out.println();
        }
    }
}
