package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _08__ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < 2 * numberN + 3; row++) {
            if (row == 0 || row == numberN + 1 || row == 2 * numberN + 2) {
                for (int col = 0; col < 2 * numberN + 3; col++) {
                    if (col == 0 || col == 2 * numberN + 2 || col == numberN + 1) {
                        System.out.print("+");
                    }
                    else {
                        System.out.print("=");
                    }
                }
                System.out.println();
            }
            else {
                for (int col = 0; col < 2 * numberN + 3; col++) {
                    if (col == 0 || col == 2 * numberN + 2 || col == numberN + 1) {
                        System.out.print("|");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}
