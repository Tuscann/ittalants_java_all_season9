package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _05_c {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = Integer.parseInt(scan.nextLine());

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < 2 * n - 2 * row - 1; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * row + 1; col++) {
                if (col != 2 * row) {
                    System.out.print("# ");
                } else {
                    System.out.println("#");
                }
            }
        }
        for (int row = 0; row < n - 1; row++) {
            for (int col = 0; col < 2 * row + 3; col++) {
                System.out.print(" ");
            }
            for (int col = 0; col < 2 * n - 3 - 2 * row; col++) {
                if (col != 2 * n - 4 - 2 * row) {
                    System.out.print("# ");
                } else {
                    System.out.println("#");
                }
            }
        }
    }
}
