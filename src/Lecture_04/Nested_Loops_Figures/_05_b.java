package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _05_b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = Integer.parseInt(scan.nextLine());

        int before = n / 2;
        int after = n / 2;

        for (int row = 0; row < n / 2 + 1; row++) {
            for (int i = 0; i < before; i++) {
                System.out.print("  ");
            }
            for (int i = 0; i < n - before - after; i++) {
                System.out.print("# ");
            }
            before--;
            after--;
            System.out.println();
        }
    }
}


