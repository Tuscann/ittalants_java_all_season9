package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _04_h_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write integer number : ");
        int n = Integer.parseInt(scan.nextLine());

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row == 0 || row == n - 1 || row + col + 1 == n || row == col) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
