package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _07_b {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number N:");
        int numberN = sc.nextInt();

        for (int row = 0; row < numberN; row++) {
            int counter = 1;
            for (int col = 0; col <= row; col++) {
                System.out.print(counter + " ");
                counter = counter * (row - col) / (col + 1);
            }
            System.out.println();
        }
    }
}
