package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _04_b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int rows = 0; rows < n; rows++) {
            for (int col = 0; col < n; col++) {
                if (rows < col) {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
    }
}
