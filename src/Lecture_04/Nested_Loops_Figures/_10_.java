package Lecture_04.Nested_Loops_Figures;

import java.util.Scanner;

public class _10_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        for (int rows = 1; rows <= n; rows++) {
            for (int cols = 1; cols <= 10; cols++) {
                System.out.print(rows * cols);

                if (cols != 10) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
