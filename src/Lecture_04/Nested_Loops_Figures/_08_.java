package Lecture_04.Nested_Loops_Figures;

public class _08_ {
    public static void main(String[] args) {

        for (int rows = 0; rows <= 8; rows++) {
            if (rows % 4 == 0) {
                System.out.println("+===+===+");
            } else {
                System.out.println("|   |   |");
            }
        }
    }
}
