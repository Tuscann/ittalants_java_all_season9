package Lecture_13.Season_7_Test_1;

import java.util.Arrays;
import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Write a sentance");
        String first = scan.nextLine();
        System.out.println("Write a sentance");
        String second = scan.nextLine();

        String[] left = findDiference(first, second);
        System.out.println();

        System.out.println("Word are : " + Arrays.toString(left));
    }

    private static String[] findDiference(String first, String second) {
        String[] firstArray = first.split(" ");
        String[] secondArray = second.split(" ");

        int n = firstArray.length;
        int m = secondArray.length;
        int counter = 0;

        for (int i = 0; i < n; i++) {
            int j;

            for (j = 0; j < m; j++) {
                if (firstArray[i].equals(secondArray[j])) {
                    break;
                }
            }
            if (j == m) {
                counter++;
            }
        }
        String[] left = new String[counter];
        int a = 0;


        for (int i = 0; i < n; i++) {
            int j;

            for (j = 0; j < m; j++) {
                if (firstArray[i].equals(secondArray[j])) {
                    break;
                }
            }
            if (j == m) {
                left[a] = firstArray[i];
                a++;
                System.out.print(firstArray[i] + " ");
            }

        }
        return left;
    }
}
