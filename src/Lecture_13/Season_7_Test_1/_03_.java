package Lecture_13.Season_7_Test_1;

public class _03_ {
    public static void main(String[] args) {
        int[] interArray = new int[]{2, 4, 5, 6, 7, 8, 9, 1, 2, 3, 5, 9, 9, 9};

        int n = 5;
        int product = calculate(interArray, n);
        int x = sumEvenNoRecursion(interArray, n);
        System.out.println(product);
        System.out.println(x);
    }

    private static int calculate(int[] charArray, int n) {
        int product = 1;

        for (int i = 0; i < charArray.length; i++) {
            int currentElement = charArray[i];

            if (i % 2 != 0 && n < currentElement) {
                product += currentElement;
//                System.out.print("Current element = " + currentElement);
//                System.out.println("  Current product = " + product);
            }
        }
        return product;

    }

    private static int sumEvenNoRecursion(int[] arr, int index) {
        int sum;
        if (index == -1) {
            return 0;
        } else if (arr[index] % 2 == 0) {
            sum = arr[index] + sumEvenNoRecursion(arr, --index);
        } else {
            sum = sumEvenNoRecursion(arr, --index);
        }
        return sum;
    }
}
