package Lecture_13.Season_7_Test_1;

import java.util.Arrays;
import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char[] array = Arrays.toString(scan.nextLine().split(" ")).toCharArray();

        char[] charArray = new char[]{
                '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'
        };

        System.out.println(new String(charArray).indexOf(array[0]) != -1 );
        System.out.println(new String(charArray).indexOf(array[1]) != -1);
        System.out.println(new String(charArray).indexOf(array[2]) != -1);


        if (new String(charArray).indexOf(array[0]) != -1 || new String(charArray).indexOf(array[1]) != -1 || new String(charArray).indexOf(array[2]) != -1) {

            int firstPostion = 0;
            int secondPostion = 0;
            int thirdPostion = 0;

            for (int i = 0; i < charArray.length; i++) {
                if (array[0] == charArray[i]) {
                    firstPostion = i;
                }
                if (secondPostion == charArray[i]) {
                    secondPostion = i;
                }
                if (thirdPostion == charArray[i]) {
                    thirdPostion = i;
                }
            }
            if (firstPostion <= secondPostion && secondPostion <= thirdPostion) {
                System.out.println("Ascending order");
            } else {
                System.out.println("Not Ascending order");
            }

        } else {
            System.out.println("Invalid cards given!");
        }

    }
}
