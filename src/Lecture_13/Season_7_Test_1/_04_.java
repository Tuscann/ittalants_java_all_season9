package Lecture_13.Season_7_Test_1;

import java.util.Random;

public class _04_ {
    public static void main(String[] args) {

        int row = 9;
        int col = 9;
        char[][] matrix = new char[row][col];

        fillMatrix(matrix);
//        printMATRIX(matrix);

        int startPointX = 0;
        int startPointY = 0;
        int endPointX = 8;
        int endPointY = 8;
        fill_Start_End_Point_Walls(startPointX, startPointY, endPointX, endPointY, row, col, matrix);
        printMATRIX(matrix);

        boolean isFound = isHavePath(startPointX, startPointY, endPointX, endPointY, matrix);
        System.out.println(isFound);
    }

    private static boolean isHavePath(int startPointX, int startPointY, int endPointX, int endPointY, char[][] matrix) {

        boolean isFoundPath = true;
//
//        while(true){
//
//        }

        //   TODO recursion for 4 direction;

        return false;
    }

    private static void fill_Start_End_Point_Walls(int startPointX, int startPointY, int endPointX, int endPointY, int row, int col, char[][] matrix) {

        matrix[startPointX][startPointY] = 'S';
        matrix[endPointX][endPointY] = 'E';

//        System.out.println("Write count of walls : ");
        int countWalls = 25;


        for (int i = 0; i < countWalls; i++) {
            Random ran = new Random();
            int currentWallX = ran.nextInt(row);
            int currentWallY = ran.nextInt(col);

            if (currentWallX != startPointX && currentWallX != endPointX && currentWallY != startPointY && currentWallY != endPointY) {
                matrix[currentWallX][currentWallY] = 'W';
            }
        }
    }

    private static void fillMatrix(char[][] matrix) {
        for (int rows = 0; rows < matrix.length; rows++) {
            for (int cols = 0; cols < matrix.length; cols++) {
                matrix[rows][cols] = ' ';
            }
        }
    }

    private static void printMATRIX(char[][] matrix) {

        System.out.println(" 012345678");
        for (int row = 0; row < matrix.length; row++) {
            System.out.print(row);
            for (int col = 0; col < matrix.length; col++) {
                System.out.print(matrix[row][col]);
            }
            System.out.print(row);
            System.out.println();
        }
        System.out.println(" 012345678");
    }
}
