package Lecture_13.Season_6_Test_1;

import java.util.Random;

public class _04_ {
    public static void main(String[] args) {
        char[][] chessboard = new char[8][8];

        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard[0].length; j++) {
                chessboard[i][j] = ' ';
            }
        }

        Random rand = new Random();

//        int horseX = rand.nextInt(8);
//        int horseY = rand.nextInt(8);
//        int kingX = rand.nextInt(8);
//        int kingY = rand.nextInt(8);


        int horseX = 0;
        int horseY = 0;
        int kingX = 6;
        int kingY = 2;

        chessboard[horseX][horseY] = 'H';
        chessboard[kingX][kingY] = 'K';
        System.out.println();
        printChessboard(chessboard);


        boolean isFoundRight = isLecalMove("right", horseX, horseY, kingX, kingY, chessboard);
//        boolean isFoundLeft = isLecalMove("left", horseX, horseY, kingX, kingY, chessboard);
//        boolean isFoundDown = isLecalMove("down", horseX, horseY, kingX, kingY, chessboard);
//        boolean isFoundUp = isLecalMove("up", horseX, horseY, kingX, kingY, chessboard);
        printChessboard(chessboard);
//
//
//        if (isFoundDown || isFoundLeft || isFoundRight || isFoundUp) {
//            System.out.println("Knight can take the king");
//        } else {
//            System.out.println("Knight can take the king");
//        }
    }

    private static boolean isLecalMove(String right, int horseX, int horseY, int kingX, int kingY, char[][] chessboard) {

        if (chessboard[horseX + 1][horseY + 2] == ' ' && chessboard.length >= horseX + 1 && chessboard.length >= horseY + 2) {
            chessboard[horseX + 1][horseY + 2] = 'H';
            chessboard[horseX + 1][horseY + 1] = 'H';
            chessboard[horseX + 1][horseY] = 'H';
            chessboard[horseX][horseY] = 'L';



        }
        if (chessboard[horseX - 1][horseY + 2] == ' ' && 0 <= horseX - 1 && chessboard.length - 1 >= horseY + 2) {
            chessboard[horseX - 1][horseY + 2] = 'H';
            chessboard[horseX - 1][horseY + 1] = 'H';
            chessboard[horseX - 1][horseY] = 'H';
            chessboard[horseX][horseY] = 'L';

        }
        return false;
    }


    private static void printChessboard(char[][] chessboard) {
        System.out.println(" ABCDEFGH");
        for (int i = 0; i < chessboard.length; i++) {
            System.out.print(i);
            for (int j = 0; j < chessboard.length; j++) {
                System.out.print(chessboard[i][j]);
            }
            System.out.println(i + "");
        }
        System.out.println(" ABCDEFGH");
    }
}
