package Lecture_13.Season_6_Test_1;

public class _03_ {
    public static void main(String[] args) {
        int[] arrayIntegers = new int[]{3, 4, 5, 1, 5, 3, -7, 112, 50};

        boolean isPositive = checkForPositive(arrayIntegers, 0);
        System.out.println(isPositive);
    }

    private static boolean checkForPositive(int[] arrayIntegers, int i) {
        if (i < arrayIntegers.length) {
            if (arrayIntegers[i] < 0) {
                return false;
            } else {
                return checkForPositive(arrayIntegers, i + 1);
            }
        }
        return true;
    }
}
