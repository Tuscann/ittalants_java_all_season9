package Lecture_13.Season_6_Test_1;

import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char[] array = new char[]{
                '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'
        };

        System.out.println("Write two cards with space : ");
        while (true) {
            String[] x = scan.nextLine().split(" ");
            char first = x[0].charAt(0);
            char second = x[1].charAt(0);

            if (first == 'A' && second == 'A') {
                System.out.println("Two cards are same A");
                break;
            }

            boolean isfound1 = false;
            boolean isfound2 = false;

            for (int i = 0; i < array.length; i++) {
                if (array[i] == first) {
                    isfound1 = true;
                }
                if (array[i] == second) {
                    isfound2 = true;
                }
            }
            if (isfound1 && !isfound2 || !isfound1 && isfound2 || !isfound1 && !isfound2) {
                System.out.println("Invalid cards given! ");
            }
        }
    }
}
