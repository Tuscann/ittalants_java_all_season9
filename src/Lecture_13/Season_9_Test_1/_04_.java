package Lecture_13.Season_9_Test_1;

import java.util.Arrays;
import java.util.Scanner;

public class _04_ {
    public static void main(String[] args) {
        int[] array = new int[]{4, 6, 7, 8, 9, 10, 13, 4, 5, 23, 567, 6, 8, 9, 10};
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        System.out.println("Unsorted array = "+Arrays.toString(array));
        //TODO sorting Algorithm
        BubbleSort(array);

        System.out.println(n);
        System.out.println("Sorted array"+Arrays.toString(array));
        System.out.printf("Biggiest %d = %d",n,array[n]);

    }

    private static void BubbleSort(int[] bubbleSort) {
        int counterSwaps = 0;
        int totalIteration = 0;
        int element;
        int size = bubbleSort.length;

        for (int i = 1; i < size; i++) {
            boolean flag = true;
            for (int j = size - 1; j >= i; j--) {
                if (bubbleSort[j - 1] > bubbleSort[j]) {
                    flag = false;
                    element = bubbleSort[j - 1];
                    bubbleSort[j - 1] = bubbleSort[j];
                    bubbleSort[j] = element;
                    counterSwaps++;
                }
                totalIteration++;
            }
            if (flag) {
                break;
            }
        }
//        System.out.println("Counter swaps = " + counterSwaps);
//        System.out.println("Iteration counter = " + totalIteration);
    }
}
