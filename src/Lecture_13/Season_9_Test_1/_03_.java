package Lecture_13.Season_9_Test_1;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number : ");
        int n = scan.nextInt();

        int[] array = new int[]{1, 4, 5, 16, 7, -23, -9, -10, -19, 8, 8, 9, 10};

        for (int i = 0; i < array.length; i++) {
            if (array[i] < n) {
                System.out.println(array[i]);
            }
        }
    }
}
