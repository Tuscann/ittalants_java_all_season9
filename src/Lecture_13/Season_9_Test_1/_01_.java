package Lecture_13.Season_9_Test_1;

import java.util.Random;

public class _01_ {
    public static void main(String[] args) {

        int totalAttempts = 0;
        int counter7 = 0;

        Random rand = new Random();

        while (true) {
            int firstDice = rand.nextInt(6) + 1;
            int secondDice = rand.nextInt(6) + 1;

            System.out.print("First dice = " + firstDice + " ");
            System.out.println("Second dice = " + secondDice);

            if (firstDice + secondDice == 7) {
                counter7++;
                System.out.println(counter7);
            }
            if (counter7 == 30) {
                break;
            }
            totalAttempts++;
        }
        System.out.print("Total totalAttempts = " + totalAttempts + " ");
    }
}
