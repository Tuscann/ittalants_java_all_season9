package Lecture_13.Season_5_Test_1;

import java.util.Random;

public class _04_ {
    public static void main(String[] args) {
        int n = 10;

        char[][] matrix = new char[n][n];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                matrix[row][col] = ' ';
            }
        }
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            int x = rand.nextInt(10);
            int y = rand.nextInt(10);
            matrix[x][y] = '*';
        }
        printMatrix(matrix);
        countMines(matrix);
        printMatrix(matrix);
    }

    private static void countMines(char[][] matrix) {
        for (int row = 0; row <= matrix.length; row++) {
            for (int col = 0; col <= matrix[0].length; col++) {
                int counter = 0;
                if (matrix[row + 1][col] == '*' && matrix.length >= row + 1) {
                    counter++;
                }
                if (matrix[row - 1][col] == '*' && 0 <= row - 1) {
                    counter++;
                }
                if (matrix[row][col + 1] == '*' && matrix.length >= col + 1) {
                    counter++;
                }
                if (matrix[row][col - 1] == '*') {
                    counter++;
                }
                if (matrix[row + 1][col + 1] == '*') {
                    counter++;
                }
                if (matrix[row - 1][col - 1] == '*') {
                    counter++;
                }
                if (matrix[row - 1][col + 1] == '*') {
                    counter++;
                }
                if (matrix[row + 1][col - 1] == '*') {
                    counter++;
                }
                matrix[row][col] = (char) counter;
            }
        }
    }

    private static void printMatrix(char[][] matrix) {
        System.out.println(" 0123456789");
        for (int row = 0; row < matrix.length; row++) {
            System.out.print(row);
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.print(matrix[row][col]);
            }
            System.out.println(row);
        }
        System.out.println(" 0123456789");
    }
}
