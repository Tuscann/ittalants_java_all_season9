package Lecture_13.Season_5_Test_1;

import java.util.Random;
import java.util.Scanner;

public class _04_Mines {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Write width : ");
        int m = Integer.parseInt(scan.nextLine());
        System.out.println("Write height : ");
        int n = Integer.parseInt(scan.nextLine());
        System.out.println("Write number of bombs : ");
        int p = Integer.parseInt(scan.nextLine());
        String[][] bombs = CreateMatrix(m, n);
        AddBombs(m, n, p, bombs);


        PrintMatrix(m, n, bombs);

        // sol[i][j] = # bombs adjacent to cell (i, j)
        int[][] sol = new int[m][n];
        for (int row = 1; row <= m - 1; row++) {
            for (int col = 1; col <= n - 1; col++) {

                for (int ii = row - 1; ii < row + 1; ii++) {         // (ii, jj) indexes neighboring cells
                    for (int jj = col - 1; jj < col + 1; jj++) {
                        if (bombs[ii][jj].equals("*")) {
                            sol[row][col]++;
                        }
                    }
                }
            }
        }


        for (int row = 0; row < sol.length; row++) {
            for (int col = 0; col < sol[0].length; col++) {
                System.out.print(sol[row][col]);
            }
            System.out.println();
        }
        System.out.println();
        PrintMatrix(m, n, bombs);
    }

    private static void PrintMatrix(int m, int n, String[][] bombs) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(bombs[i][j]);
            }
            System.out.println();
        }
    }

    private static void AddBombs(int m, int n, int p, String[][] bombs) {
        for (int k = 0; k < p; k++) {
            Random rand = new Random();
            int x = rand.nextInt(m);
            int y = rand.nextInt(n);
            bombs[x][y] = "*";
        }
    }

    private static String[][] CreateMatrix(int m, int n) {
        // game grid is [1..m][1..n], border is used to handle boundary cases
        String[][] bombs = new String[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                bombs[i][j] = ".";
            }
        }
        return bombs;
    }
}
