package Lecture_13.Season_5_Test_1;

import java.util.Random;

public class _01_ {
    public static void main(String[] args) {

        int counterTotal = 0;
        int after = 0;
        int previosSum = 0;

        while (true) {
            System.out.println("Write a number between 1 and 6");
            Random rand = new Random();
            int dice1 = 1 + rand.nextInt(6);
            int dice2 = 1 + rand.nextInt(6);

            System.out.println("Dice 1 = " + dice1 + " \nDice 2 = " + dice2);

            if (dice1 + dice2 == 7) {

                if (previosSum == 7) {
                    after++;
                }
                if (after == 7) {
                    System.out.println("Total attempts : " + counterTotal);
                    break;
                }
                previosSum = 7;
            } else {
                previosSum = 0;
                after = 0;
            }
            counterTotal++;
        }
    }
}
