package Lecture_13.Season_5_Test_1;

import java.util.Arrays;

public class _03_ {
    public static void main(String[] args) {
        int[] array = new int[]{-31, -30, -15, -12, -7, - 4, 5, 6, 7, 8, 9};

        int[] newArray = printArray(array);
        System.out.println(Arrays.toString(newArray));

    }

    private static int[] printArray(int[] array) {

        int counter = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 0) {
                counter = i;
                break;
            }
        }
        int[] newArray = new int[counter];

        for (int i = 0; i < counter; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }
}
