package Lecture_13.Season_5_Test_1;

import java.util.Random;

public class _04_MInesss {

    private int mWidth = 4;      // width of the minefield
    private int mHeight = 4;     // height of the minefield
    private int mMines = 7;      // number of mines
    private char[][] mMinefield; // 2-dimensional array of chars for our board

    public _04_MInesss() {
        System.out.println("Generating minefield...");

        mMinefield = new char[mHeight][mWidth];

        System.out.println("Clearing minefield...");

        fillingMinefieldWithFreeSpaces();

        System.out.println("Placing mines...");

        placeMines();
        drawMinefield();

        System.out.println("Calculating hints...");

        calculateHints();
        drawMinefield();
    }

    public void placeMines() {
        int minesPlaced = 0;
        Random random = new Random(); // this generates random numbers for us
        while (minesPlaced < mMines) {
            int x = random.nextInt(mWidth); // a number between 0 and mWidth - 1
            int y = random.nextInt(mHeight);

            if (mMinefield[y][x] != '*') {    // make sure we don't place a mine on top of another
                mMinefield[y][x] = '*';
                minesPlaced++;
            }
        }
    }

    public void fillingMinefieldWithFreeSpaces() {
        // Set every grid space to a space character.
        for (int y = 0; y < mHeight; y++) {
            for (int x = 0; x < mWidth; x++) {
                mMinefield[y][x] = ' ';
            }
        }
    }

    public void drawMinefield() {
        for (int y = 0; y < mHeight; y++) {

            System.out.print(y + " ");

            for (int x = 0; x < mWidth; x++) {
                System.out.print(mMinefield[y][x]);
            }

            System.out.print(" "+y);

            System.out.print("\n");
        }
    }

    public void calculateHints() {
        for (int y = 0; y < mHeight; y++) {
            for (int x = 0; x < mWidth; x++) {
                if (mMinefield[y][x] != '*') {
                    mMinefield[y][x] = minesNear(y, x);
                }
            }
        }
    }

    public char minesNear(int y, int x) {
        int mines = 0;
        // check mines in all directions
        mines += mineAt(y - 1, x - 1);  // NW
        mines += mineAt(y - 1, x);      // N
        mines += mineAt(y - 1, x + 1);  // NE
        mines += mineAt(y, x - 1);      // W
        mines += mineAt(y, x + 1);      // E
        mines += mineAt(y + 1, x - 1);  // SW
        mines += mineAt(y + 1, x);      // S
        mines += mineAt(y + 1, x + 1);  // SE
        if (mines > 0) {
            // we're changing an int to a char
            // why?!
            // http://www.asciitable.com/
            // 48 is ASCII code for '0'
            return (char) (mines + 48);
        } else {
            return ' ';
        }
    }

    // returns 1 if there's a mine a y,x or 0 if there isn't
    public int mineAt(int y, int x) {
        // we need to check also that we're not out of array bounds as that would
        // be an error
        if (y >= 0 && y < mHeight && x >= 0 && x < mWidth && mMinefield[y][x] == '*') {
            return 1;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        _04_MInesss mineSweeper = new _04_MInesss();
    }
}
