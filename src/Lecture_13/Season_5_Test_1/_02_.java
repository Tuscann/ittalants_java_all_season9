package Lecture_13.Season_5_Test_1;

import java.sql.SQLOutput;
import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a string : ");
        String[] x = scan.nextLine().split("");

        String xa = countBIgger(x);
        System.out.println("Ratio = " + xa);
    }

    private static String countBIgger(String[] inputString) {
        int totalLters = inputString.length;
        int counterSmallLetters = 0;
        int counterBigLetters = 0;

        for (String curentChar : inputString) {
            if (curentChar.toUpperCase().equals(curentChar)) {
                counterBigLetters++;
            } else {
                counterSmallLetters++;
            }
        }

        return "" + (int) (((totalLters / 100.0) * counterSmallLetters) * 10) +" : "+ (int) (((totalLters / 100.0) * counterBigLetters) * 10);
    }
}
