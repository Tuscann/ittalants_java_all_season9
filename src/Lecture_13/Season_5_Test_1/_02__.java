package Lecture_13.Season_5_Test_1;

public class _02__ {
    public static void main(String[] args) {
        String text = "Zdravei kak si tii MdsdasdIR";

        ratioUpperCaseLowerCase(text);
    }
    private static void ratioUpperCaseLowerCase(String text) {
        int countUpperCaseLetters = 0;
        int countLowerCaseLetters = 0;

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) >= 'a' && text.charAt(i) <= 'z') {
                countLowerCaseLetters++;
            }
            if (text.charAt(i) >= 'A' && text.charAt(i) <= 'Z') {
                countUpperCaseLetters++;
            }
        }
        int gcd = findGCD(countUpperCaseLetters, countLowerCaseLetters);

        if (countUpperCaseLetters < countLowerCaseLetters) {
            System.out.println("The ratio between all lower case letters and all upper case letters is: "
                    + (countUpperCaseLetters / gcd) + ":" + (countLowerCaseLetters / gcd));
        }
        else {
            System.out.println("The ratio between all upper case letters and all lower case letters is: "
                    + (countLowerCaseLetters / gcd) + ":" + (countUpperCaseLetters / gcd));
        }
    }

    private static int findGCD(int number1, int number2) {
        //base case
        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1 % number2);
    }
}
