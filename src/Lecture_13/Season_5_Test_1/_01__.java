package Lecture_13.Season_5_Test_1;

import java.util.Random;

public class _01__ {
    public static void main(String[] args) {

        Random rand = new Random();

        System.out.println("Roll the dice");

        int sumOfTwoDice = 0;
        long countOfRollDice = 0;
        int countOfSeqDices = 0;

        while (true) {
            sumOfTwoDice = (rand.nextInt(6) + 1) + (rand.nextInt(6) + 1);
            countOfRollDice++;
            if (sumOfTwoDice == 7) {
                countOfSeqDices++;
                if (countOfSeqDices == 7) {
                    break;
                }
            } else {
                countOfSeqDices = 0;
            }
        }
        System.out.println(countOfRollDice);
    }
}
