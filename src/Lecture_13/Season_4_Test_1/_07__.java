package Lecture_13.Season_4_Test_1;

import java.util.Scanner;

public class _07__ {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int x = scan.nextInt();
        int y = scan.nextInt();

        System.out.println(basepowerexp(x, y));
    }

    public static int basepowerexp(int base, int exp) {
        if (exp == 0) {
            return 1;
        } else {
            return base * basepowerexp(base, exp - 1);
        }
    }
}
