package Lecture_13.Season_4_Test_1;

import java.util.Scanner;

public class _09_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number");
        int n = scan.nextInt();
        int sum = 0;

        System.out.println("Sum of Fibonacci" + " numbers is : " + calculateSum(n));
    }

    private static int calculateSum(int n) {

        int fibo[] = new int[n + 1];
        fibo[0] = 0;
        fibo[1] = 1;
        System.out.print(fibo[0]+" "+fibo[1]+" ");

        // Initialize result
        int sum = 0;

        // Add remaining terms
        for (int i = 2; i < n; i++) {
            fibo[i] = fibo[i - 1] + fibo[i - 2];
            if (fibo[i] > n) {
                break;
            }
            System.out.print(fibo[i] + " ");
            sum += fibo[i];
        }
        System.out.println();

        return sum;
    }
}
