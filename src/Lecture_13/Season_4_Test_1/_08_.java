package Lecture_13.Season_4_Test_1;

public class _08_ {
    public static void main(String[] args) {

        String[] array = new String[]{
                "Welcome", "home", "in", "aa", "kamen"
        };
        int length = array[0].length();


        for (int i = 1; i < array.length; i++) {
            if (array[i].length() > length) {
                length = array[i].length();
            }
        }
        System.out.println(length);
        for (int i = 0; i < length + 2; i++) {
            System.out.print("*");
        }
        System.out.println();

        for (String anArray : array) {
            System.out.print("*" + anArray);
            for (int cols = anArray.length(); cols < length; cols++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }
        for (int i = 0; i < length + 2; i++) {
            System.out.print("*");
        }
        System.out.println();


    }
}
