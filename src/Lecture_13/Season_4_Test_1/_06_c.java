package Lecture_13.Season_4_Test_1;

import java.util.Scanner;

public class _06_c {
    public static void main(String[] args) {
        System.out.println("Write a number : ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        boolean isFound = true;

        for (int i = 1; i <= n / 2; i++) {
            if (n % i == 0 && i * i * i == n) {
                System.out.println("Number " + n + " is cube of " + i);
                isFound = false;
                break;
            }
        }
        if (isFound) {
            System.out.println("Number " + n + " is not cube.");
        }

    }
}
