package Lecture_13.Season_4_Test_1;

import java.util.Scanner;

public class _07_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write a number and stepen");

        String[] input = scan.nextLine().split(" ");

        int number = Integer.parseInt(input[0]);
        int stepen = Integer.parseInt(input[1]);


        int sum = calculate(number, stepen);
        System.out.println("Sum = " + sum);
    }

    private static int calculate(int number, int stepen) {
        if (stepen - 1 > 0) {
            number *= calculate(number, stepen - 1);
        }
        return number;
    }
}
