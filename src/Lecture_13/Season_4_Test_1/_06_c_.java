package Lecture_13.Season_4_Test_1;

import java.util.Scanner;

public class _06_c_{
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            int number = sc.nextInt();
            isCubeNum(number);
        }

        private static void isCubeNum(int number) {
            boolean isCubeNumber = false;
            int cubeN = 0;
            for (int i = 2; i <= number / 2; i++) {
                if (number % i == 0) {
                    if (i * i * i == number) {
                        isCubeNumber = true;
                        cubeN = i;
                        break;
                    }
                }
            }
            if (isCubeNumber) {
                System.out.println(number + " is a cube of " + cubeN);
            }
            else {
                System.out.println(number + " is not a cube");
            }
        }
}
