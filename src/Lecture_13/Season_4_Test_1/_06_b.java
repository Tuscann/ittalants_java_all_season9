package Lecture_13.Season_4_Test_1;

public class _06_b {
    public static void main(String[] args) {
        int balance = 20;
        while (balance >= 1) {
            if (balance < 9) {
                System.out.println(2);
                continue;
            }
            balance = balance - 9;
            System.out.println(1);
        }
    }
}
