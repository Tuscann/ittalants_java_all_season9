package Lecture_06;

public class _07_ {
    public static void main(String[] args) {
        int[][] matrix = {
                {11, 12, 13, 14, 15, 16},
                {21, 22, 23, 24, 25, 26},
                {31, 32, 33, 34, 35, 36},
                {41, 42, 43, 44, 45, 46},
                {51, 52, 53, 54, 55, 56},
                {61, 62, 63, 64, 65, 66}
        };

        long bugSum = 0;

        for (int row = 0; row < matrix.length; row++) {
            long sumRow = 0;
            int collll = 0;
            for (int cols = 0; cols < matrix[0].length; cols++) {
                if (((row + 1 + cols + 1) % 2) == 0) {
                    System.out.print(matrix[row][cols] + " ");
                    sumRow += matrix[row][cols];
                    collll = cols;
                }
            }
            if (((row + collll + 2) % 2) == 0) {
                System.out.printf("= %d\n", sumRow);
            }
            bugSum += sumRow;
        }
        System.out.println("Sum of all elements on odd rows = " + (bugSum));
    }
}
