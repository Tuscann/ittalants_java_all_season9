package Lecture_06;

public class _07__ {
    public static void main(String[] args) {
        int[][] matrix = {
                {11, 12, 13, 14, 15, 16},
                {21, 22, 23, 24, 25, 26},
                {31, 32, 33, 34, 35, 36},
                {41, 42, 43, 44, 45, 46},
                {51, 52, 53, 54, 55, 56},
                {61, 62, 63, 64, 65, 66}
        };

        int sumRow = 0;
        long bugSum = 0;
        int row = 0;
        int col = -1;

        for (int i = 0; i < matrix.length * matrix.length; i++) {
            col++;
            if ((row + col) % 2 == 0) {
                sumRow += matrix[row][col];
                System.out.print(matrix[row][col] + ", ");
            }
            if (col == matrix.length - 1) {
                row++;
                col = -1;
                System.out.println("Sum of row's elements is: " + sumRow);
                bugSum += sumRow;
                sumRow = 0;
            }
        }
        System.out.println("Sum of all elements on odd rows = " + (bugSum));
    }
}
