package Lecture_06;

import java.util.Scanner;

public class _01_ {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int rows = 6;
        int cols = 5;

        int[][] matrix = new int[rows][cols];

        matrix = new int[][]{
                {48, 72, 13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, 33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
                {61, 69, 63, 64, 65},
        };

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
//                System.out.print("Add integer number for row " + row + " and column " + col + " : ");
                int current = matrix[row][col];
                if (min > current) {
                    min = current;
                }
                if (max < current) {
                    max = current;
                }
                matrix[row][col] = current;
            }
        }
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.print(matrix[row][col]+" ");
            }
            System.out.println();
        }
        System.out.println("Smallest number in matrix : " + min);
        System.out.println("Biggest number in matrix : " + max);
    }
}
