package Lecture_06;

import java.util.Scanner;

public class _03_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Write rows for the matrix : ");
        int rows = Integer.parseInt(scan.nextLine());
        System.out.print("Write cols for the matrix : ");
        int cols = Integer.parseInt(scan.nextLine());

        int matrix[][] = new int[rows][cols];
        double sum = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.print("Add integer number for row " + row + " and column " + col + " : ");
                int current = Integer.parseInt(scan.nextLine());
                sum += current;
                matrix[row][col] = current;
            }
        }
        double average = sum / (rows * cols);

        System.out.println("Sum of all elements of the matrix is  : " + sum);
        System.out.println("Average of all elements of the matrix is : " + average);
    }
}
