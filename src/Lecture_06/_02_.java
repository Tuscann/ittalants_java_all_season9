package Lecture_06;

import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[][] matrix = new int[][]{
                {48, 72, 13, 14, 15},
                {21, 22, 53, 24, 75},
                {31, 57, 33, 34, 35},
                {41, 95, 43, 44, 45},
                {59, 52, 53, 54, 55},
        };

        int[] mainDiagonal = new int[matrix.length];
        int[] otherDiagonal = new int[matrix.length];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {

                if (row == col) {
                    mainDiagonal[row] = matrix[row][col];
                }
                if (row == matrix[0].length - col - 1) {
                    otherDiagonal[row] = matrix[row][matrix.length - 1 - row];
                }
            }
        }
        System.out.println("Main diagonal : ");
        for (int i = 0; i < mainDiagonal.length; i++) {
            System.out.print(mainDiagonal[i] + " ");
        }
        System.out.println();
        System.out.println("Secondary diagonal : ");
        for (int i = 0; i < otherDiagonal.length; i++) {
            System.out.print(otherDiagonal[i] + " ");
        }
    }
}
