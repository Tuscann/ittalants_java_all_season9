package Lecture_06._Extra_Tasks;

import java.util.Scanner;

public class _02_ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Write integer for rows and columns the matrix : ");
        int rows = Integer.parseInt(scan.nextLine());

        int matrix[][] = new int[rows][rows];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.print("Add integer number for row " + row + " and column " + col + " : ");

                matrix[row][col] = Integer.parseInt(scan.nextLine());
            }
        }
        System.out.println("Reversed array on 90 degrees.");
        for (int col = 0; col < matrix.length; col++) {
            for (int row = matrix[0].length - 1; row >= 0; row--) {
                if (row == 0) {
                    System.out.print(matrix[row][col]);
                } else {
                    System.out.print(matrix[row][col] + ", ");
                }
            }
            System.out.println();
        }
    }
}
