package Lecture_06._Extra_Tasks;

public class _01_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {16, 3, 2, 13},
                {5, 10, 11, 8},
                {9, 6, 7, 12},
                {4, 15, 14, 1}
        };
        int firstDiagonalSum = 0;
        int secondDiagonalSum = 0;
        int currentRowSum = 0;
        int nextRowSum = 0;

        for (int row = 0; row < matrix.length; row++) {
            currentRowSum = 0;
            for (int col = 0; col < matrix[0].length; col++) {
                if (row == col) {
                    firstDiagonalSum += matrix[row][col];
                }
                if (row == matrix.length - 1 - col) {
                    secondDiagonalSum += matrix[row][col];
                }
                currentRowSum += matrix[row][col];
            }
            if (row == 0) {
                nextRowSum = currentRowSum;
            }
            if (currentRowSum != nextRowSum) {
                break;
            }
        }

        if (currentRowSum == firstDiagonalSum && firstDiagonalSum == secondDiagonalSum) {
            System.out.println("Magic square, sum of rows and cols = " + firstDiagonalSum);
        }
        else {
            System.out.println("The sums of all rows, all cols, first diagonal and second diagonal are not equal.");
        }
    }
}
