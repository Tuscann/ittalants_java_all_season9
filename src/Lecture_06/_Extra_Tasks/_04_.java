package Lecture_06._Extra_Tasks;

public class _04_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {16, 3, 2, 13, 4},
                {5, 10, 11, 8, 4},
                {9, 6, 7, 12, 5},
                {4, 15, 14, 1, 8}
        };
        int rows = matrix.length;
        int cols = matrix[0].length;

        if (rows > cols) {
            int[][] newMatrix1 = new int[rows][rows];
            for (int row = 0; row < newMatrix1.length; row++) {
                for (int col = 0; col < newMatrix1[row].length; col++) {
                    if (col < cols) {
                        newMatrix1[row][col] = matrix[row][col];
                    } else {
                        newMatrix1[row][col] = 0;
                    }

                    if (col == newMatrix1.length - 1) {
                        System.out.println(newMatrix1[row][col]);
                    } else {
                        System.out.print(newMatrix1[row][col] + " ");
                    }
                }
            }
        } else if(rows < cols) {
            int[][] newMatrix2 = new int[cols][cols];
            for (int row = 0; row < newMatrix2.length; row++) {
                for (int col = 0; col < newMatrix2[row].length; col++) {
                    if (row < rows) {
                        newMatrix2[row][col] = matrix[row][col];
                    } else {
                        newMatrix2[row][col] = 0;
                    }
                    if (col == newMatrix2.length - 1) {
                        System.out.println(newMatrix2[row][col]);
                    } else {
                        System.out.print(newMatrix2[row][col] + " ");
                    }
                }
            }
        }
    }
}
