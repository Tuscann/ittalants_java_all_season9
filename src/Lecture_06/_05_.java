package Lecture_06;

public class _05_ {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };

        long maxSumRows = 0;
        long maxSumColumns = 0;

        for (int row = 0; row < matrix.length; row++) {
            int currentSumRow = 0;
            int currentSumColumns = 0;
            for (int col = 0; col < matrix[0].length; col++) {
                currentSumRow += matrix[row][col];
                currentSumColumns += matrix[col][row];
            }
            if (currentSumRow > maxSumRows) {
                maxSumRows = currentSumRow;
            }
            if (currentSumColumns > maxSumColumns) {
                maxSumColumns = currentSumColumns;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Max sum of rows = " + maxSumRows);
        System.out.println("Max sum of Columns = " + maxSumColumns);

        if (maxSumRows > maxSumColumns) {
            System.out.println("The max sum of the rows is bigger then max sum of the columns: " + maxSumRows + " > " + maxSumColumns);
        } else if (maxSumRows < maxSumColumns) {
            System.out.println("The max sum of the columns is bigger then max sum of the rows: " + maxSumColumns + " > " + maxSumRows);
        } else {
            System.out.println("Max sum of the rows is equal to max sum of the columns: " + maxSumRows);
        }
    }
}
