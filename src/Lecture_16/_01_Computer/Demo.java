package Lecture_16._01_Computer;


public class Demo {
    public static void main(String[] args) {
        Computer firstComputer = new Computer();
        Computer secondComputer = new Computer(1980, 564.12, 120.45, 12.34);
        Computer noteBook = new Computer(1990, 890.4, true, 345.67, 45.7, "Linux");

        System.out.println(secondComputer);


        int price = noteBook.comparePrice(secondComputer.getPrice());
        if (price == 1) {
            System.out.println("NoteBook has bigger prise than Second Computer");
        } else if (price == -1) {
            System.out.println("NoteBook has bigger prise than Second Computer");
        } else if (price == 0) {
            System.out.println("Computers have same price.");
        }

    }
}
