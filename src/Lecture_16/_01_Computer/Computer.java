package Lecture_16._01_Computer;

public class Computer {
    private int year;
    private double price;
    private boolean isNotebook = false;
    private double hardDiskMemory;
    private double freeMemory;
    private String operationSystem = "Win XP";

    public Computer() {

    }

    public Computer(int year, double price, double hardDiskMemory, double freeMemory) {
        this.year = year;
        this.price = price;
        this.hardDiskMemory = hardDiskMemory;
        this.freeMemory = freeMemory;
    }

    public Computer(int year, double price, boolean isNotebook, double hardDiskMemory, double freeMemory, String OPSystem) {
        this.year = year;
        this.price = price;
        this.isNotebook = isNotebook;
        this.hardDiskMemory = hardDiskMemory;
        this.freeMemory = freeMemory;
        this.operationSystem = OPSystem;
    }

    @Override
    public String toString() {
        return "Building year = " + year + "\nPrice = " + price + "\nIsNotebook = " + isNotebook + "\nTotal hardDisk space = " + hardDiskMemory + "\nFreeMemory = " + freeMemory + "\nOperation System : " + operationSystem;
    }

    public void changeOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    public void useMemory(double memory) {
        if (this.freeMemory < memory) {
            System.out.println("Not enough free memory!");
        } else {
            this.freeMemory -= memory;
        }
    }

    public int comparePrice(double priceSecondComputer) {
        if (this.price > priceSecondComputer) {
            return 1;
        } else if (this.price < priceSecondComputer) {
            return -1;
        }
        return 0;
    }

    public double getPrice() {
        return price;
    }
}
