package Lecture_16.Car_Person_CarShop;

public class Car {
    private String modelParam;
    private double price;
    private boolean isSportCar;
    private String color;
    private int gear;
    private double currentSpeed;
    private final double MAXSPEED = 220;

    public Car(boolean isSportCar, String coloParam) {
        setIsSportCar(isSportCar);
        setColor(coloParam);
        setGear(4);
        setCurrentSpeed(45);
        setPrice(45);
    }

    public Car(String modelParam, double price, boolean isSportCar, String color, int gear, double currentSpeed) {
        setModelParam(modelParam);
        setPrice(price);
        setIsSportCar(isSportCar);
        setColor(color);
        setGear(gear);
        setCurrentSpeed(currentSpeed);
    }

    public void setCurrentSpeed(double currentSpeed) {
        if (currentSpeed < MAXSPEED) {
            this.currentSpeed = currentSpeed;
        }
    }

    boolean isMoreExpensive(Car car) {

        if (car.price > this.price) {
            return false;
        } else {
            return true;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setModelParam(String modelParam) {
        this.modelParam = modelParam;
    }

    public double calculateCarPriceForScrap(double metalPrice) {

        double coefiicent = 0.2;
        if (color.equals("white") || color.equals("black")) {
            coefiicent += 0.05;
        }
        if (isSportCar) {
            coefiicent += 0.05;
        }
        return metalPrice * coefiicent;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setIsSportCar(boolean sportCar) {
        isSportCar = sportCar;
    }

    public String getModelParam() {
        return modelParam;
    }

    @Override
    public String toString() {
        return "Model " + this.modelParam + "\nIs sport Car " + this.isSportCar + "\nPrice : " + this.price + "\nColor : " + color + "\nGear : " + this.gear + "\nCurrentSpeed : " + this.currentSpeed+"\n";
    }
}
