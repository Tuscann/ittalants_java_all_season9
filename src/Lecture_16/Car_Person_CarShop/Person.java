package Lecture_16.Car_Person_CarShop;

public class Person {
    private int age;
    private String name;
    private double height;
    private double weight;
    private long personalNumber;
    private boolean isMale;
    private double money;
    private Car myCar;
    // private Person[] FriendsArray;


    public Person() {
        setAge(0);
        setWeight(4.0);
//        Person[] FriendsArray = new Person[3];
//        for (int i = 0; i < FriendsArray.length; i++) {
//
//        }

    }

    public Person(String name, long personalNumber, boolean isMale) {
        setName(name);
        setPersonalNumber(personalNumber);
        setMale(isMale);
    }

    public Person(int age) {
        this();
        setAge(age);
    }

    public Person(int age, String name) {
        this(age);
        setName(name);
    }

    public Person(int age, String name, double height) {
        this(age, name);
        setHeight(height);
    }

    void buyCar(Car car) {
        if (this.money > car.getPrice()) {
            System.out.printf("%s buy %s for %.2f $ .\n", this.name, car.getModelParam(), car.getPrice());
            setOwner(car);
            car.setPrice(car.getPrice() - this.money);
            System.out.printf("%s left with %.2f $ .\n", this.name, this.money);
            this.money = 0;

        } else {
            System.out.printf("%s not have enough money.\n", this.name);
        }
    }

    public void setOwner(Car myCar) {
        this.myCar = myCar;
    }

    public void setPersonalNumber(long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public void changeOwner(Person newOwner) {
        newOwner.setOwner(this.myCar);
        myCar = null;

    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setAge(int age) {
        if (age >= 0 && age <= 130) {
            this.age = age;
        } else {
            System.out.println("Age between 0 and 130");
        }
    }

    public void setName(String name) {
        if (name.length() > 0 && name.length() < 120) {
            this.name = name;
        } else {
            System.out.println("Name between 0 and 130 chars");
        }
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double sellCarForScrap() {
        double price = 0.0;

        if (myCar != null) {
            price = myCar.calculateCarPriceForScrap(12);
            myCar = null;
        }
        return price;
    }

    @Override
    public String toString() {
        if (myCar != null) {
            return "Name : " + name + "\nAge : " + age + "\nHeight : " + height + "\nWeight : " + weight + "\nPersonal Number : " + personalNumber + "\nIs male : " + isMale + "\nMy car : " + myCar.getModelParam() + "\n";
        } else {
            return "Name : " + name + "\nAge : " + age + "\nHeight : " + height + "\nWeight : " + weight + "\nPersonal Number : " + personalNumber + "\nIs male : " + isMale + "\n";
        }

    }
}
