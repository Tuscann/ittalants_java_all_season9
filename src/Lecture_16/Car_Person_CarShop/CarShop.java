package Lecture_16.Car_Person_CarShop;

public class CarShop {

    private Car[] arrayNEW;
    private int capacity;

    CarShop(int capacity) {
        setCapacity(capacity);
        arrayNEW = new Car[capacity];
    }

    boolean addCar(Car car) {

        if (this.capacity > 0) {
            arrayNEW[arrayNEW.length - capacity] = car;
            capacity--;
            return true;
        } else {
            return false;
        }
    }

    Car getNextCar() {
        return arrayNEW[arrayNEW.length - capacity];
        //TODO tests 3 methods
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    void sellNextCar(Person buyer) {

        Car x = arrayNEW[arrayNEW.length - capacity];

        buyer.buyCar(x);
        removeCar(x);
    }

    void removeCar(Car car) {
        arrayNEW[arrayNEW.length - capacity] = null;
    }


    void showAllCarsInTheShop() {
        System.out.println("Capacity " + capacity);
        for (int i = 0; i < arrayNEW.length; i++) {
            if (arrayNEW[i] != null) {
                Car x = arrayNEW[i];

                System.out.println(x);
            }
        }
    }

}
