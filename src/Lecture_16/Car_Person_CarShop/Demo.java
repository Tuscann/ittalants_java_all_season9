package Lecture_16.Car_Person_CarShop;

public class Demo {
    public static void main(String[] args) {
        Person ivancho = new Person(12, "Ivancho", 2.02);
        Person marcheto = new Person();
        Person kamencho = new Person(12, "Kamencho", 1.90);
        Person pamela = new Person(42, "Pamela", 1.97);
        Person naomi = new Person("Naomi", 54690878, false);

        Car ferrari = new Car("Ferrari 6050", 450000.100, true, "red", 4, 160);
        Car trabant = new Car("Trabant 112", 2100.00, false, "white", 1, 50);
        Car BMV = new Car("BMW X5", 2800.00, false, "green", 3, 150);
        Car Kia = new Car("KIA best", 16100.00, true, "white", 1, 120);
        Car Volvo = new Car("Volvo 45", 4100.00, true, "black", 2, 170);
        Car magare = new Car(false, "red");

        boolean expensive = ferrari.isMoreExpensive(trabant);

        if (expensive) {
            System.out.println("Ferrari is more expensive");
        } else {
            System.out.println("Trabant 601 is more expensive");
        }

        double metalPrice = trabant.calculateCarPriceForScrap(8);
        System.out.println("MetalPrice for trabant = " + metalPrice);

        ivancho.buyCar(trabant);
        ivancho.setMoney(3000);
        ivancho.buyCar(trabant);
        System.out.println(ivancho);

        marcheto.setMoney(3000);
        pamela.setMoney(3000);
        ivancho.changeOwner(pamela);
        double moneyfromScrap = pamela.sellCarForScrap();
        System.out.printf("\nMoney from scrap : %.2f\n", moneyfromScrap);

        System.out.println(ivancho);
        System.out.println(pamela);
//        System.out.println(kamencho);


        CarShop bestCarShop = new CarShop(6);
        bestCarShop.addCar(ferrari);
        bestCarShop.addCar(trabant);
        bestCarShop.addCar(BMV);
        bestCarShop.addCar(Kia);
        bestCarShop.addCar(Volvo);


        bestCarShop.showAllCarsInTheShop();
    }
}

