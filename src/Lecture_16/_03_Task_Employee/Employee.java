package Lecture_16._03_Task_Employee;

public class Employee {
    private String name;
    private Task currentTask;
    private int hoursLeftForToday;

    public Employee(String name) {
        setName(name);
    }

    public void setName(String name) {
        if (name.length() > 0 && name.length() <= 30) {
            this.name = name;
        }
    }

    void work() {
        if (currentTask.getWorkingHours() > this.hoursLeftForToday) {
            currentTask.setWorkingHours(currentTask.getWorkingHours() - this.hoursLeftForToday);
            setHoursLeftForToday(0);
        } else {
            this.hoursLeftForToday -= currentTask.getWorkingHours();
            currentTask.setWorkingHours(0);
        }
    }

    public void setHoursLeftForToday(int hoursLeftForToday) {
        if (hoursLeftForToday >= 0) {
            this.hoursLeftForToday = hoursLeftForToday;
        }
    }

    public void setTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public String getName() {
        return this.name;
    }

    void showReport() {
        System.out.println("Worker name = " + this.name);
        System.out.println("Task name = " + currentTask.getName());
        System.out.printf("Working Hours left for %s = %d\n", getName(), this.hoursLeftForToday);
        System.out.println("Task working hours left = " + currentTask.getWorkingHours());
    }
}
