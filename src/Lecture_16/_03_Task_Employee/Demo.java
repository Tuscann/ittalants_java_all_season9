package Lecture_16._03_Task_Employee;

public class Demo {
    public static void main(String[] args) {
        Employee Ivan = new Employee("Ivan");
        Employee Kamen = new Employee("Kamen");
        Employee Meri = new Employee("Meri");

        Ivan.setHoursLeftForToday(8);
        Kamen.setHoursLeftForToday(10);
        Meri.setHoursLeftForToday(7);

        Task copyTask = new Task("Copy", 1);
        Task pasteTask = new Task("Paste", 1);
        Task runningTask = new Task("Running", 40);

        Ivan.setTask(copyTask);
        Ivan.work();
        Ivan.setTask(pasteTask);
        Ivan.work();
        Ivan.setTask(runningTask);
        Kamen.setTask(runningTask);
        Meri.setTask(runningTask);

        Ivan.work();
        Kamen.work();
        Meri.work();

        Meri.showReport();
    }
}
