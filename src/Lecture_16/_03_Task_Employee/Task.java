package Lecture_16._03_Task_Employee;

public class Task {
    private String name;
    private int workingHours;

    public Task(String name, int workingHours) {
        setName(name);
        setWorkingHours(workingHours);
    }

    public void setName(String name) {
        if (name.length() > 0) {
            this.name = name;
        }
    }

    public void setWorkingHours(int workingHours) {
        if (workingHours >= 0 && workingHours <= 168) {
            this.workingHours = workingHours;
        }
    }

    public int getWorkingHours() {
        return workingHours;
    }

    public String getName() {
        return this.name;
    }
}
