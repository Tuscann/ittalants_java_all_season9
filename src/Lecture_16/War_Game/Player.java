package Lecture_16.War_Game;

import java.util.Random;

public class Player {
    private String name;
    Card[] cards = new Card[26];
    private int sumCards = 0;

    public Player(String name) {
        this.name = name;
        Random r = new Random();
        for (int i = 0; i < cards.length; i++) {
            int cardPower = 1+r.nextInt(12);
            cards[i] = new Card(cardPower);
        }
    }

    public String getName() {
        return name;
    }

    public int getSumCards() {
        return sumCards;
    }

    public void setSumCards(int sumCards) {
        this.sumCards = sumCards;
    }
}
