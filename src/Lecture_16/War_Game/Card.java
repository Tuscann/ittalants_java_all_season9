package Lecture_16.War_Game;

public class Card {
    private int power;
    private String suit;

    public Card(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }
}
