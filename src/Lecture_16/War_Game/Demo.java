package Lecture_16.War_Game;

public class Demo {
    public static void main(String[] args) {
        Player ivan = new Player("Ivancho");
        Player mimi = new Player("Mimi");
        int counter = 0;
        for (int i = 0; i < 26; i++) {
            if (ivan.cards[i].getPower() > mimi.cards[i].getPower()) {
                ivan.setSumCards(ivan.getSumCards() + 2);
            } else if (ivan.cards[i].getPower() < mimi.cards[i].getPower()) {
                mimi.setSumCards(mimi.getSumCards() + 2);
            } else if (ivan.cards[i].getPower() == mimi.cards[i].getPower()) {

                System.out.printf("Ivan card power %d\n", ivan.cards[i].getPower());
                System.out.printf("Mimi card power %d\n", mimi.cards[i].getPower());

                System.out.println("---------- START A WAR -------------");

                recursion(ivan, mimi, counter);

                System.out.println();
            }
        }
        System.out.printf("%s total cards : %d\n", ivan.getName(), ivan.getSumCards());
        System.out.printf("%s total cards : %d\n", mimi.getName(), mimi.getSumCards());
    }

    private static void recursion(Player ivan, Player mimi, int counter) {

        counter += 1;
        int position = counter * 3;

        System.out.printf("At position %d Ivan card power %d\n", position, ivan.cards[position].getPower());
        System.out.printf("At position %d Mimi card power %d \n", position, mimi.cards[position].getPower());
        if (ivan.cards[counter * 3].getPower() > mimi.cards[position].getPower() && position < ivan.cards.length) {
            ivan.setSumCards(ivan.getSumCards() + counter * 2);
            System.out.printf("%s win the war\n", ivan.getName());
        } else if (ivan.cards[position].getPower() < mimi.cards[position].getPower() && position < mimi.cards.length) {
            System.out.printf("%s win the war\n", mimi.getName());
            mimi.setSumCards(mimi.getSumCards() + counter * 2);
        } else if (ivan.cards[position].getPower() == mimi.cards[position].getPower() && position < mimi.cards.length) {
            recursion(ivan, mimi, counter);
        }
    }
}

