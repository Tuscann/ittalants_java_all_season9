package Lecture_16._02_Student;

public class Collage {
    public static void main(String[] args) {
        Student ivancho = new Student();
        Student penka = new Student("Penka", "Kamenar", 25);
        Student marche = new Student("Marche", "Informatics", 20, 4.56, 3, false, 34.6);
        Student mimito = new Student("Mimito", "Informatics", 27, 3.56, 1, false, 10.0);
        Student kamncho = new Student("Kamencho", "Informatics", 35, 3.56, 1, false, 12.0);
        Student Zhivko = new Student("Zhivko", "Informatics", 45, 5.80, 2, false, 24.0);
        Student Penelope_Kruz = new Student("Penelope", "Informatics", 6, 4.00, 1, false, 12.0);
        Student minka = new Student("Minka", "Informatics", 25, 4.12, 2, false, 20.0);

        double moneyFromScholarShip = minka.receiveSholarship(4.03, 12);
        System.out.println("Money after receiving scholarship = " + moneyFromScholarShip);

        System.out.println(ivancho);
        System.out.println(penka);
        marche.upYear();
        marche.upYear();
        System.out.println();

        StudentGroup bestStudentGroup = new StudentGroup("Informatics");

        bestStudentGroup.addStudent(marche);
        bestStudentGroup.addStudent(mimito);
        bestStudentGroup.addStudent(penka);
        bestStudentGroup.addStudent(kamncho);
        bestStudentGroup.addStudent(Zhivko);
        bestStudentGroup.addStudent(minka);
        bestStudentGroup.addStudent(Penelope_Kruz);
        bestStudentGroup.addStudent(Penelope_Kruz);
        bestStudentGroup.addStudent(Penelope_Kruz);

        bestStudentGroup.printStudentsInGroup();

        System.out.println("Name of best student : "+ bestStudentGroup.theBestStudent());

        bestStudentGroup.emptyGroup();
        System.out.println("Print empty group : ");
        bestStudentGroup.printStudentsInGroup();
    }
}
