package Lecture_16._02_Student;

public class StudentGroup {
    private String groupSubject;
    private Student[] AllStudents;
    private int freePlaces;

    public StudentGroup() {
        this.AllStudents = new Student[5];
        this.freePlaces = 5;
    }

    public StudentGroup(String subject) {
        this();
        this.groupSubject = subject;
    }

    void addStudent(Student a) {
        if (a.getSubject().equals(groupSubject) && freePlaces >= 1) {
            this.freePlaces -= 1;
            AllStudents[AllStudents.length - freePlaces - 1] = a;
        } else if (!a.getSubject().equals(groupSubject)) {
            System.out.printf("%s are not part of subject %s\n", a.getName(), groupSubject);
        } else {
            System.out.printf("Sorry %s try next semester again! Full group\n", a.getName());
        }
    }

    void emptyGroup() {
        this.AllStudents = new Student[5];
        this.freePlaces = 5;
    }

    public String theBestStudent() {
        if (AllStudents.length > 0) {
            double bestGrade = this.AllStudents[0].getGrade();
            int bestGradeStudentIndex = 0;

            for (int i = 1; i < AllStudents.length; i++) {
                if (AllStudents[i].getGrade() > bestGrade) {
                    bestGrade = AllStudents[i].getGrade();
                    bestGradeStudentIndex = i;
                }
            }
            return this.AllStudents[bestGradeStudentIndex].getName();
        } else {
            return "The " + groupSubject + " group is empty.";
        }
    }

    void printStudentsInGroup() {
        for (int i = 0; i < AllStudents.length; i++) {
            Student x = AllStudents[i];
            if (x != null) {
                System.out.println("Student number = " + (i + 1));
                System.out.println(x.toString());
            }
        }
    }
}
