package Lecture_16._02_Student;

public class Student {
    private String name;
    private String subject;
    private double grade;
    private int yearInCollege;
    private int age;
    private boolean isDegree;
    private double money;

    public Student() {
        this.grade = 4.0;
        this.yearInCollege = 1;
        this.isDegree = false;
        this.money = 0;
    }

    public Student(String name, String subject, int age) {
        this();
        this.name = name;
        this.subject = subject;
        if (age > 0 && age < 130) {
            this.age = age;
        }
    }


    public Student(String name, String subject, int age, double grade, int yearInCollege, boolean isDegree, double money) {
        this.grade = grade;
        this.yearInCollege = yearInCollege;
        this.isDegree = false;
        this.money = money;
        this.name = name;
        this.subject = subject;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Name : " + name + "\nSubject : " + subject + "\nGrade : " + grade + "\nYears in college : " + yearInCollege + "\nAge : " + age + "\nIs Degree : " + isDegree + "\nMoney : " + money + "\n";
    }

    void upYear() {
        if (!this.isDegree) {
            this.yearInCollege += 1;
            if (yearInCollege == 4) {
                isDegree = true;
            }
        } else {
            System.out.printf("Student %s has a degree.",this.name);
        }
    }

    double receiveSholarship(double min, double amount) {
        if (this.grade >= min && this.age < 30) {
            this.money += amount;
        }
        return this.money;
    }

    public String getSubject() {
        return this.subject;
    }

    public double getGrade() {
        return grade;
    }

    public String getName() {
        return name;
    }
}
